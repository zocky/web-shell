import React from "react";
import { observable, computed, action, reaction } from "mobx";
import { observer } from "mobx-react";
import { DesktopWindow } from "./DesktopWindow";

@observer
export class AppWindow extends React.Component {

  // TODO: move to app store

  render() {
    const { window, app = window } = this.props;
    return (
      <DesktopWindow window={window}>
        <AppFrame app={app || window} window={window}></AppFrame>
      </DesktopWindow>
    )
  }
}


@observer
class AppFrame extends React.Component {
  @computed get window() {
    return this.props.window;
  }
  @computed get app() {
    return this.props.app;
  }

  render() {
    return (
      <iframe
        sandbox="allow-scripts allow-forms allow-same-origin allow-popups allow-popups-to-escape-sandbox"
        data-app-state={this.app.appState}
        data-wid={this.window.wid}
        ref={el => { this.app.iframe = el; }}
        src={this.app.realUrl}
        onLoad={this.app.loaded}
      />
    )
  }
}