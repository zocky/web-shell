import React from "react";
import { observable, computed, action, reaction } from "mobx";
import { observer } from "mobx-react";
import { ContextMenuProvider } from './ContextMenu';
import { ShellContext } from "./ShellContext";

@observer
export class Dock extends React.Component {
  static contextType = ShellContext;

  @computed
  get groupIcons () {
    const {desktop} = this.context;
    return desktop.registry.get('desktop','dock','group-icons');
  }
  set groupIcons (val) {
    const {desktop} = this.context;
    return desktop.registry.set('desktop','dock','group-icons',val);
  }


  @action.bound
  getMenuOptions() {
    return [{
      label: this.groupIcons ? "Ungroup App Windows" : "Group App Windows",
      execute: ()=>this.groupIcons = !this.groupIcons
    }]
  }

  @computed get windowGroups() {
    const {desktop} = this.context;
    const ret = {}
    for (const window of Object.values(desktop.windows)) {
      if (!ret[window.group]) {
        ret[window.group] = {leader:null,members:[],group:window.group}
      } 
    }
    for (const wid of desktop.orderedWindows) {
      const window = desktop.windows[wid];
      if (window.is.hidden) continue;
      if (!ret[window.group].leader) ret[window.group].leader = window;
      ret[window.group].members.push(window);
    }
    return Object.values(ret);
  }

  render() {
    const {desktop} = this.context;
    return (
      <ContextMenuProvider getOptions={this.getMenuOptions}>
        <div className="dock">
          <MainMenuIcon/>
          {!this.groupIcons && Object.values(desktop.windows).filter(w=>!w.is.hidden).map(w => <DockIcon key={w.wid} window={w}/> )}
          {this.groupIcons && this.windowGroups.map(g => <DockGroupIcon key={g.group} {...g} window={g.leader}/> )}
        </div>
      </ContextMenuProvider>
    )
  }
}


@observer class MainMenuIcon extends React.Component {
  static contextType = ShellContext;

  @computed get isActive() {
    return this.context.desktop.showMainMenu;
  }
  classes = () => {
    var c = "icon"
    if (this.isActive) c+=" active";
    return c;
  }
  onMouseDown = () => {
    this.context.desktop.showMainMenu = !this.context.desktop.showMainMenu;
  }
  render() {
    return (
      <div className={this.classes()} title={this.props.title} onMouseDown={this.onMouseDown}>
        <img src="/assets/icons/shell-white.svg"/>
      </div>
    )
  }
}



@observer class DockGroupIcon extends React.Component {
  classes = () => {
    const {is,isActive,wid} = this.props.leader;

    var c = "icon"
    if (is.minimized) c+=" minimized";
    if (is.maximized) c+=" maximized";
    if (isActive) c+=" active";
    return c;
  }
  onMouseDown = (e) => {
    if (e.button!=0) return;
    const { minimize, isActive, focus } = this.props.leader;
    this.props.leader.desktop.showMainMenu = false;
    if (isActive) {
      if (this.props.members.length>1) this.props.members[1].focus();
      else minimize();
      
    }
    else focus();
  }
  render() {
    const {members,leader,group} = this.props;
    if (members.length<1) return null;
    return (
      <ContextMenuProvider getOptions={()=>[{label:'Close '+group,execute:leader.close}]}>
        <div className={this.classes()} title={leader.title} onMouseDown={this.onMouseDown}>
          <img src={leader.icon}/>
        </div>
      </ContextMenuProvider>
    )
  }
}


@observer class DockIcon extends React.Component {
  classes = () => {
    const {is,isActive,wid} = this.props.window;

    var c = "icon"
    if (is.minimized) c+=" minimized";
    if (is.maximized) c+=" maximized";
    if (isActive) c+=" active";
    return c;
  }
  onMouseDown = (e) => {
    if (e.button!==0) return;
    const { minimize, isActive, focus } = this.props.window;
    this.props.window.desktop.showMainMenu = false;
    if (isActive) minimize();
    else focus();
  }
  render() {
    const {title,window} = this.props;
    return (
      <ContextMenuProvider getOptions={()=>[{label:'Close '+window.title,execute:window.close}]}>
        <div className={this.classes()} title={title} onMouseDown={this.onMouseDown}>
          <img src={window.icon}/>
        </div>
      </ContextMenuProvider>
    )
  }
}

