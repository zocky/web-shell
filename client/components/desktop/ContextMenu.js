import React from "react";
import { observable, computed, action, reaction } from "mobx";
import { observer } from "mobx-react";

const MenuContext = React.createContext(null);

export class ContextMenuProvider extends React.Component {
  static contextType = MenuContext;

  reportOptions = (e,childOptions) => {
    const myOptions = this.props.getOptions ? this.props.getOptions().concat() : [];
    const options = [...childOptions,...myOptions];
    this.context.reportOptions(e,options);
  }

  onContextMenu = e => {
    console.log('cmenu',e.shiftKey);
    e.preventDefault();
    e.stopPropagation();
    this.reportOptions(e,[]);

  }
  render () {
    var Element=this.props.as || 'div';
    return (
      <MenuContext.Provider value={this}>
        <Element style={{display:"contents"}} onContextMenu={this.onContextMenu}>
          {this.props.children}
        </Element>
      </MenuContext.Provider>
    )
  }
}

@observer
export class ContextMenu extends React.Component {
  @observable showMenu = false;
  @observable options = [];
  @observable x = 0;
  @observable y = 0;

  @action.bound
  hide () {
    this.showMenu = false;
  }

  @action.bound
  reportOptions(e,options) {
    console.log(e.clientX,e.clientY)
    this.x = e.clientX;
    this.y = e.clientY;

    this.options=options;
    this.showMenu = true;
    console.log('show',options)
  }
  render () {
    return (
      <MenuContext.Provider value={this}>
        {this.props.children}
        {this.showMenu && <Menu provider={this}/>}
      </MenuContext.Provider>
    )
  }
}

@observer
class Menu extends React.Component {
  render () {
    var {x,y,options,hide} = this.props.provider;
    return (
      <div className="context-menu" tabIndex="-1" ref={el=>el && el.focus()} style={{top:y,left:x}} onBlur={hide}>
        {options.map(({label,execute},i)=>(
          <div key={i} className="option" onClick={()=>{hide();execute();} }>
            {label}
          </div>
        ))}
      </div>
    )
  }
}