import React from "react";
import { toJS, observable, computed, action, reaction, get } from "mobx";
import { observer } from "mobx-react";

import { ShellContext } from "./ShellContext";

import { MainMenu } from "./MainMenu"
import { AppWindow } from "./AppWindow"
import { FileManagerWindow } from "./FileManagerWindow"
import { DialogFileOpen, DialogFileSave, DialogConfirm } from "./Dialog";
import { ContextMenu, ContextMenuProvider } from './ContextMenu';
import { Dock } from './Dock';
import { Panel } from './Panel';

const dialogs = {
  'file-open': DialogFileOpen,
  'file-save': DialogFileSave,
  'confirm': DialogConfirm
}

@observer
export class Desktop extends React.Component {

  @computed get desktop() {
    return this.props.desktop;
  }
  @computed get shell() {
    return this.props.desktop;
  }
  renderDialog() {
    const dialog = this.desktop.dialog;
    if (!dialog || !dialogs[dialog.type]) return null;
    const Dialog = dialogs[dialog.type];
    return <Dialog desktop={this.desktop} {...dialog} />
  }

  render() {
    const { shell, desktop } = this.props;

    return (
      <ShellContext.Provider value={{ shell, desktop }}>
        <ContextMenu>
          <div className="shell">
            <Dock desktop={desktop} />
            <div className="main">
              <div className="desktop">
                {Object.values(desktop.windows).map(window => {
                  switch (window.windowType) {
                    case 'fs':
                    case 'app': return <AppWindow key={window.wid} window={window} />
                    case 'filemanager': return <FileManagerWindow key={window.wid} window={window} />
                    default: throw 'unknown window type ' + window.windowType;
                  }
                })}
              </div>
              {
                desktop.showMainMenu && <MainMenu desktop={desktop} />
              }
              {
                this.renderDialog()
              }
            </div>
            <Panel desktop={desktop} />
            {
              desktop.dragging && <div id="overlay" style={{ cursor: desktop.dragging }} />
            }
          </div>
        </ContextMenu>
      </ShellContext.Provider>
    )
  }
}

