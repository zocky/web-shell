import React from "react";
import { observable, computed, action, reaction, get, values } from "mobx";
import { observer, disposeOnUnmount } from "mobx-react";

@observer
export class Folder extends React.Component {
  @observable path = null;
  @observable error = null;
  @observable rawFiles = [];

  @observable selectedFile = null;

  @computed
  get pathParts() {
    var p = '';
    const ret = [{
      path: "/",
      name: "/"
    }];
    this.path && this.path.split('/').filter(Boolean).forEach(part => {
      p += '/' + part;
      ret.push({
        name: part,
        path: p
      })
    });
    return ret;
  }

  @observable renamingFile = true;
  @observable creatingFolder = false;

  @action.bound
  createFolder() {
    this.selectedFile = null;
    this.creatingFolder = true;
  }

  @computed get files() {
    return this.rawFiles.concat();
  }

  @computed get
    filterFile() {
    return file => {
      const include = this.props.include;
      if (file.is.folder) return true;
      if (!include || !include.length) return true;
      return include.some(filter => file.match(filter))
    }
  }

  @computed
  get sortedFiles() {
    var files = this.files.concat().filter(this.filterFile);
    files.sort((a, b) => (+!!b.is.folder - !!a.is.folder || +(a.name > b.name) || -(a.name < b.name)));
    files = files.filter(file => !file.is.hidden);
    return files;
  }

  constructor(props, context) {
    super(props, context);

    reaction(

      () => this.path,

      async path => {
        this.props.onChangePath && this.props.onChangePath(path)
        try {
          this.selectedFile = null;
          this.rawFiles = [];
          const files = await this.props.desktop.fs.ls(path);
          this.rawFiles = files;
          this.error = null;
        } catch (error) {
          console.error(error);
          this.error = error;
          this.rawFiles = [];
        }
      }
    );

    reaction(
      () => this.selectedFile,
      file => file ? this.props.onSelect && this.props.onSelect(this.selectedFile) : this.props.onDeselect && this.props.onDeselect()
    )

    reaction(
      () => this.props.selected,
      selected => this.selectedFile = this.files.find(file => file.name === selected)
    )
    this.path = this.props.path || "";
  }

  @observable view = 'list';

  @action.bound
  toggleView() {
    if (this.view === 'list') this.view = 'icons';
    else this.view = 'list'
  }
  render() {
    function ToolbarButton({ icon, label, onClick }) {
      return (
        <span
          className="button"
          onClick={onClick}
        >
          <i className={"ion-md-" + icon}></i>
          <span className="label">{label}</span>
        </span>
      )
    }
    return (
      <div className="folder">
        <div className="sh toolbar">
          <div className="path-buttons">
            {
              this.pathParts.map(part => {
                return <button key={part.path} onMouseDown={() => this.path = part.path}>{part.name}</button>
              })
            }
          </div>
          <div className="sh toolbar">
            {
              this.pathParts.length == 2 && 
              <ToolbarButton 
                label="Settings" 
                icon="settings" 
                onClick={() => {
                  this.props.desktop.fs.mtab[this.pathParts[1].name].app.focus();
                }}
              />
            }
            {
              this.pathParts.length > 1 && 
              <ToolbarButton 
                label="New Folder" 
                icon="folder" 
                onClick={this.createFolder}
              />
            }
            <ToolbarButton 
              label="" 
              icon={this.view === 'icons' ? 'reorder' : 'apps'} 
              onClick={this.toggleView}
            />
          </div>
        </div>
        {
          this.error ? (
            <div className="error">
              <pre>{JSON.stringify(this.error, null, 2)}</pre>
            </div>
          ) : (
              <div className={"sh files list"} data-view={this.view}>
                <div className="header">
                  <span className="icon"></span>
                  <span className="name">Name</span>
                  <span className="extra">Type</span>
                  <span className="right details size">Size</span>
                </div>
                <div className="items">
                  {
                    this.sortedFiles.map(file => (
                      <FileEntry
                        key={file.id || file.path}
                        file={file}
                        selected={this.selectedFile && this.selectedFile.name === file.name}
                        onDoubleClick={() => {
                          if (file.is.folder) {
                            this.path = file.path;
                          } else {
                            this.props.onDoubleClick && this.props.onDoubleClick(file)
                          }
                        }}
                        onClick={() => {
                          this.selectedFile = file;
                        }}
                      />
                    ))
                  }
                </div>
              </div>
            )
        }
      </div>
    )
  }
}

@observer
class FileEntry extends React.Component {
  fileSize(file) {
    if (typeof file.size != "number") return null;
    if (file.size >= 1E9) return +(+(file.size / (1E9)).toPrecision(3)).toFixed(1) + ' G';
    if (file.size >= 1E6) return +(+(file.size / (1E6)).toPrecision(3)).toFixed(1) + ' M';
    if (file.size >= 1E3) return +(+(file.size / (1E3)).toPrecision(3)).toFixed(1) + ' K';
    return file.size + ' B';
  }
  @observable renaming = false;
  @observable renamingTo = "";

  @action.bound
  onNameClick() {

    if (this.props.selected) {
      this.renaming = true;
      this.renamingTo = this.props.file.name;
    }

  }

  render() {
    const { file, selected } = this.props;
    return (
      <div
        className={"file item" + (selected ? " selected" : "")}
        onClickCapture={e => {
          e.preventDefault();
          e.stopPropagation();
          this.props.onClick && this.props.onClick()
        }}
        onDoubleClick={this.props.onDoubleClick}
      >
        <span className="icon">
          <FileIcon file={file} />
        </span>
        {
          this.renaming
            ? <input className="rename" type="text" value={this.renamingTo} onChange={e => { this.renamingTo = e.target.value }} autoFocus onBlur={() => this.renaming = false} />
            :
            <>
              <span className="name" onClick={selected ? this.onNameClick : null}>{file.name}</span>
              <span className="type extra">{file.type}</span>
            </>
        }
        <span className="right details size">{this.fileSize(file)}</span>
      </div>

    )
  }
}

@observer
class FileIcon extends React.Component {

  defaultTypes = {
    text: "plain",
    folder: "directory",
    video: "x-generic",
    application: "octet-stream"
  }

  typeIconUrl(mainType, subType) {
    return new URL(`/assets/icons/Humanity/mimes/24/${mainType}-${subType}.svg`, window.origin)
  }

  @computed
  get iconUrl() {
    const type = this.props.file.type;
    if (!type) return "";
    const [mainType, subType] = type.split('/');
    return this.typeIconUrl(mainType, subType);
  }

  @computed
  get defaultIconUrl() {
    const type = this.props.file.type;
    if (!type) return "";
    const mainType = type.split('/')[0];
    const defaultType = this.defaultTypes[mainType];
    if (!defaultType) return this.typeIconUrl("application", "octet-stream");
    return this.typeIconUrl(mainType, defaultType);
  }

  @computed
  get realIconUrl() {
    return this.loadError ? this.defaultIconUrl : this.iconUrl;
  }

  @observable loadError = false;

  componentDidMount() {
    this.reactions = [
      reaction(
        () => this.props.file.types,
        (types) => this.types = types.concat()
      )
    ]
    this.types = this.props.file.types ? this.props.file.types.concat() : [this.props.file.type];
    this.type = this.types.shift();
  }
  componentWillUnmount() {
    this.reactions.forEach(r => r())
  }
  render() {
    return (
      <div className="iconset">
        <img
          src={this.realIconUrl}
          onError={() => this.loadError = true} />
      </div>
    )
  }
}
