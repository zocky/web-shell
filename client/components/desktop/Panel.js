import React from "react";
import { toJS, observable, computed, action, reaction, get } from "mobx";
import { observer, disposeOnUnmount } from "mobx-react";
import { ShellContext } from "./ShellContext";
import { Panes } from "../panes";

@observer
export class Panel extends React.Component {
  panes = Panes;

  @observable
  activePane = null;

  @computed get Pane() {
    return this.activePane && this.panes[this.activePane] && this.panes[this.activePane].Pane
  }

  @action.bound
  openPane(id) {
    this.activePane = id;
  }

  @action.bound
  closePane() {
    this.activePane = null;
  }

  @action.bound
  togglePane(id) {
    if (id === this.activePane) this.activePane = null;
    else this.activePane = id;

  }

  render() {
    const Pane = this.Pane;
    return (
      <div className={"panes" + (Pane ? " expanded" : " collapsed")}>
        <div className="panel">
          {Object.entries(this.panes).map(([id, { Indicator }]) => (
            <div 
              key={ id } 
              className={ id + " indicator" + (this.activePane===id?" active":"") } 
              onClick={ () => this.togglePane(id) }
            >
              <Indicator panel={this} desktop={this.props.desktop} />
            </div>
          ))}
        </div>
        {Pane && (
          <div className={this.activePane + " pane flex-column"}>
            <Pane panel={this} desktop={this.props.desktop} />
          </div>
        )}
      </div>
    )
  }
}
