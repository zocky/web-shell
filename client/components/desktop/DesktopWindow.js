import React from "react";
import { observable, computed, action, reaction } from "mobx";
import { observer } from "mobx-react";

import { Toolbar } from "../UI";


@observer
export class DesktopWindow extends React.Component {

  @computed
  get wid() {
    return this.props.window.wid;
  }

  classes = () => {
    const { is } = this.props.window;
    var c = "window"
    if (is.minimized) c += " minimized";
    if (is.maximized) c += " maximized";
    if (this.props.window.isActive) c += " active";
    if (is.hidden) c += " hidden";
    return c;
  }

  dragHandler = (cursor, cb1, cb2, cb3) => {
    const { window } = this.props;
    return downEvent => {
      window.focus();
      if (downEvent.button !== 0) return;
      downEvent.stopPropagation();
      downEvent.preventDefault();

      const onMouseMove = moveEvent => {
        if (!window.desktop.dragging) {
          window.desktop.dragging = cursor;
          cb1 && cb1(downEvent);
        }
        moveEvent.stopPropagation();
        moveEvent.preventDefault();
        cb2 && cb2(moveEvent);
      }

      const onMouseUp = upEvent => {
        window.desktop.dragging = null;
        removeEventListener('mousemove', onMouseMove, true);
        removeEventListener('mouseup', onMouseUp, true);
        upEvent.stopPropagation();
        upEvent.preventDefault();
        cb3 && cb3(moveEvent);
      }

      addEventListener('mousemove', onMouseMove, true);
      addEventListener('mouseup', onMouseUp, true);
    }
  }

  dragWindow = this.dragHandler("default", null, event => { this.props.window.move(event.movementX, event.movementY) }, null);
  resizeWindow = this.dragHandler("se-resize", null, event => { this.props.window.resize(event.movementX, event.movementY); }, null);

  render() {
    const window = this.props.window;
    const { wid, is, x, y, w, h, z, title } = this.props.window;
    return (
      <div
        data-wid={wid}
        className={this.classes()}
        style={{ left: x, top: y, width: w, height: h, zIndex: z }}
        tabIndex={-1}
        onMouseDown={window.focus}
        ref={el => window.root = el}
      >
        <div className="titlebar">
          <span className="buttons">
            {
              window.titlebarButtons.map(button => <TitleBarButton key={button.name} {...button} />)
            }
          </span>
          <span
            className="title"
            title={window.description}
            onMouseDown={this.dragWindow}
            onDoubleClick={is.maximized ? window.unmaximize : window.maximize}
          >{title}</span>
        </div>
        <Toolbar buttons={window.toolbarButtons}/>
        {
          window.logs.length > 0 &&
          <div className="logs">
            {window.logs.map((log, i) => (
              <div key={i} className={"log " + log.type}>
                <div className="message">{log.message}</div>
                <span className="dismiss" onClick={log.dismiss}>dismiss</span>
              </div>
            ))}
          </div>
        }
        <div className="client">{this.props.children}</div>
        <div className="resizer" onMouseDown={this.resizeWindow}></div>
      </div>
    )
  }
}

class TitleBarButton extends React.Component {
  onClick = e => {
    e.preventDefault();
    e.stopPropagation();
    this.props.execute();
  }
  render() {
    return (
      <span className={this.props.name + " button"} onClick={this.onClick}>
        <i className={"ion-md-" + this.props.icon}></i>
      </span>
    )
  }
}


