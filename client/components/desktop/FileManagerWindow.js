import React from "react";
import { observable, computed, action, reaction } from "mobx";
import { observer } from "mobx-react";
import { DesktopWindow } from "./DesktopWindow";
import { Folder} from "./Folder";

@observer
export class FileManagerWindow extends React.Component {

  @observable group = "file-manager";

  render() {
    const { root = "/", path, window } = this.props;
    return (
      <DesktopWindow window={window}>
        <Folder shell={window.desktop} desktop={window.desktop} root={root} path={path} onChangePath={path=>window.title=path}/>
      </DesktopWindow>
    )
  }
}


