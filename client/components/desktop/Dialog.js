import React from "react";
import { observable, computed, action, reaction } from "mobx";
import { observer } from "mobx-react";
import { Folder } from "./Folder";

@observer 
export class Dialog extends React.Component{
  render() {
    return(
      <div className="dialog">
        <div className="content">
          <div className="header">
            <div className="title">
              {this.props.title}
            </div>
          </div>
          <ErrorBoundary>
            {this.props.children}
          </ErrorBoundary>
        </div>
      </div>
    )
  }
}



@observer 
class FileDialog extends React.Component {
  
  @observable 
  fileName = "";

  @observable 
  selectedFile = null;

  @observable 
  path = null;

  OK = () => {
    if (this.isDisabled) return;
    this.props.desktop.registry.set('dialog','file','lastPath',this.path);
    this.props.close({
      path: this.filePath,
      file: this.selectedFile,
      format: this.matchingFormats[0].format,
      extension: this.matchingFormats[0].extension
    });
  }

  cancel = () => {
    this.props.close(null);
  }

  onDeselect = () => {
    this.selectedFile = null;
    //this.fileName = "";
  }

  onSelect = (file) => {
    if (file.is.folder) {
      //this.fileName = "";
    } else {
      this.selectedFile = file;
      this.fileName = file.name
    }
  }
  
  @computed get filePath() {
    if(!this.fileName) return null;
    return this.path+'/'+this.fileName;
  }

  @computed get formats() {
    const ff = Object.values(this.props.formats||[]);
    var f = ff.map(({id,title,type,extension})=>{
      return {key:id,id,title,include:[{extension,type}]};
    })

    if (f.length > 1) f.push({
      title: "All supported files",
      include: f.map(g=>g.include[0]),
      key:"supported",
    })
    f.push({
      title: "All files",
      include: [],
      key:"all",
    })
    return f;
  }

  @computed get
  matchingFormats() {
    const check = this.selectedFile || (this.fileName ? {name:this.fileName} : null);
    if (!check) return [];
    const format = this.currentFormatFilter;
    var formats = Object.assign(this.props.formats);
    if (format) formats={[format]:formats[format]};


    var ret = [];
    for (var fid in formats) {
      var f = formats[fid];
      if (!this.props.desktop.fs.matchFile(check,f)) continue;

      if (f.extension) {
        var ee = f.extension.split(/\s+/).sort((a, b) => a.split('.').length > b.split('.').length);
        for (const e of ee) {
          if (e === '*') continue;
          if (check.name.endsWith('' + e)) {
            var n = e.split('.').length;
            ret[n] = ret[n] || [];
            ret[n].push({ format: fid, extension: e })
          }
        }
      } else {
        ret[1] = ret[1] || [];
        ret[1].push({ format: fid, extension: check.name.split('.').slice(1).pop()})
      }
    }
    console.log(ret);

    ret = ret.filter(Boolean);
    if(!ret.length) return [];
    var max = ret.pop();
    if (max.length === 1) return max;
    ret.reverse();
    return [].concat(max,...ret);
  }

  @observable currentFormatFilter="all";

  @computed get include() {
    var f = this.formats.find(f=>f.id===this.currentFormatFilter);
    return f && f.include || [];
  }

  onChangePath = (path) => {
    this.path=path;
  }

  onDoubleClick = (file) => {
    if(this.isDisabled) return;
    this.file = file;
    this.OK();
  }

  componentWillMount() {
    this.currentFormatFilter = Object.keys(this.props.formats)[0];
    this.fileName = this.props.fileName || "";
  }
  
  onChangeFileName = (e) => {
    this.fileName = e.target.value;
  }

  @computed get
  isDisabled() {
    if (!this.fileName) return true;
    if (this.readOnly && !this.selectedFile) return;
    if (this.matchingFormats.length!==1) return true;
    return false;
  }
  render() {
    const {readOnly,title,path,...rest} = this.props;
    return (
      
      <Dialog title={title}>
        <div className="filename" title={JSON.stringify(this.matchingFormats,null,2)}>
          { readOnly 
            ? <input type="text" name="filename" disabled readOnly={true} value={this.selectedFile ? this.selectedFile.name : ""}/>
            : <input type="text" name="filename" value={this.fileName} onChange={this.onChangeFileName}/>
          }
        </div>
        <Folder 
          path={path || this.props.desktop.registry.get('dialog','file','lastPath') || '/'}
          selected={this.fileName}
          onSelect={this.onSelect} 
          onDeselect={this.onDeselect} 
          onChangePath={this.onChangePath} 
          onDoubleClick={this.onDoubleClick}
          include={this.include}
          {...rest}
        />
        <div className="footer">
          <select onChange={e=>{
              this.currentFormatFilter=e.target.value;
            }} value={this.currentFormatFilter}>
            {this.formats.map(({key,id,title})=>(
              <option key={key} value={id||""}>{title}</option>
            ))}
          </select>
          <button onClick={this.cancel}>Cancel</button>
          <button disabled={this.isDisabled} onClick={this.OK}>OK</button>
        </div>
      </Dialog>
    )
  }
}

@observer 
export class DialogFileSave extends React.Component {
  render () {
    return (
      <FileDialog title="Save File" {...this.props} />
    );
  }
}

@observer 
export class DialogFileOpen extends React.Component {
  render () {
    return (
      <FileDialog title="Open File" readOnly {...this.props} />
    );
  }
}


@observer 
export class DialogConfirm extends React.Component {
  
  OK = () => {
    this.props.close(true);
  }

  cancel = () => {
    this.props.close(false);
  }

  render() {
    const {title,question} = this.props;
    return (
      <Dialog title={title}>
        <div className="question">
          {question}
        </div>
        <div className="footer">
          <select onChange={e=>{
              this.selectedFormat=e.target.value;
            }} value={this.selectedFormat}>
            {this.formats.map(({key,id,title})=>(
              <option key={key} value={id||""}>{title}</option>
            ))}
          </select>
          <button onClick={this.cancel}>Cancel</button>
          <button disabled={this.isDisabled} onClick={this.OK}>OK</button>
        </div>
      </Dialog>
    )
  }
}


class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  componentDidCatch(error, info) {
    // You can also log the error to an error reporting service
    logErrorToMyService(error, info);
  }

  render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return <h1>Something went wrong.</h1>;
    }

    return this.props.children; 
  }
}