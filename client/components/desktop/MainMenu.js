import React from "react";
import { toJS, observable, computed, action, reaction } from "mobx";
import { observer } from "mobx-react";

@observer export class MainMenu extends React.Component {
  @observable url = "";
  onKey = e => {
    if (e.key!=="Enter") return;
    this.props.desktop.showMainMenu = false;
    this.props.desktop.openApp(this.url);
  }
  render() {
    const registeredApps = Object.values(this.props.desktop.registry.get('apps'));
    const registeredFS = Object.values(this.props.desktop.registry.get('fs'));
    const mountedFS = Object.values(this.props.desktop.registry.get('fs-mounted'));
    return (
      <div className="main-menu">
        <div className="from-url">
          <input 
            autoFocus
            type="text" 
            name="url"
            placeholder="Open app from URL ..."
            value={this.url}
            onChange={e=>this.url=e.target.value}
            onKeyDown={this.onKey}
          />
        </div>
        <h2>Apps</h2>
        <div className="apps">
          {registeredApps.map((app)=>(
            <MainMenuEntry key={app.url} title={app.manifest.title} icon={app.manifest.icon} onClick={()=>{
              this.props.desktop.showMainMenu = false;
              this.props.desktop.openApp(app.url);
            }}/>
          ))}
        </div>
        <h2>Files</h2>
        <div className="apps">
          <MainMenuEntry title="File Manager" icon="/assets/icons/Humanity/mimes/64/folder-directory.svg" onClick={()=>{
            this.props.desktop.showMainMenu = false;
            this.props.desktop.openFileManager("/");
          }}/>
          {Object.entries(this.props.desktop.fs.mtab).map(([id,mp])=>(
            <MainMenuEntry key={id} title={id} icon="/assets/icons/Humanity/mimes/64/folder-remote.svg" onClick={()=>{
              this.props.desktop.showMainMenu = false;
              this.props.desktop.openFileManager("/"+id);
            }}/>
          ))}
        </div>
        <h2>File System Connectors</h2>
        <div className="apps">
          {registeredFS.map((app)=>(
            <MainMenuEntry key={app.url} title={app.manifest.title} icon={app.manifest.icon} onClick={()=>{
              this.props.desktop.showMainMenu = false;
              this.props.desktop.openFileSystem(app.url);
            }}/>
          ))}
          {Object.entries(this.props.desktop.fs.mtab).map(([id,mp])=>(
            <MainMenuEntry key={id} title={id} icon="/assets/icons/Humanity/mimes/64/folder-remote.svg" onClick={()=>{
              this.props.desktop.showMainMenu = false;
              mp.app.focus();
            }}/>
          ))}
        </div>
      </div>
    );
  }
}

@observer
class MainMenuEntry extends React.Component {
  render() {
    return (
      <div className="app" onClick={this.props.onClick}>
        <div className="icon">
          <img src={this.props.icon}/>
        </div>
        <div className="title">
          {this.props.title}
        </div>
      </div>
    )
  }
}