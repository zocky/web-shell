import React from "react";
import { observable, computed, action, reaction, get } from "mobx";
import { observer, disposeOnUnmount } from "mobx-react";
import { Howl } from "howler";

@observer 
export class Knob extends React.Component {
  static sound = new Howl({
    src: '/assets/sounds/click.ogg',
    html5: true,
    format: ['ogg'],
    volume: 0.2,
    sprite: {
      click: [100, 1000]
    }
  });

  @computed get min() { return this.props.min || 0; }
  @computed get max() { return this.props.max || 10; }
  @computed get step() { return this.props.step || 1; }

  @observable _value = 0;
  @computed get value() { return this._value }
  set value(val) {
    const { min, max, round } = Math;
    this._value = min(this.max, max(this.min, round(val / this.step) * this.step));
    console.log(this._value);
  }

  @computed
  get angle() {
    return (this.value - this.min) / (this.max - this.min) * 300 - 150
  }

  @computed get isOn() {
    return this.value > this.min;
  }

  @action.bound
  up() {
    this.value += this.step;
  }

  @action.bound
  down() {
    this.value -= this.step;
  }

  @action.bound
  onWheel(e) {
    if (e.deltaY > 0) this.down();
    else this.up();
  }

  @disposeOnUnmount
  disposers1 = reaction(
    () => this.value,
    value => this.props.onChange && this.props.onChange(value)
  )
  @disposeOnUnmount
  disposer2 = reaction(
      () => this.isOn,
      isOn => {
        isOn ? this.props.onOn && this.props.onOn() : this.props.onOff && this.props.onOff();
        this.constructor.sound.play('click');
      }
    )
  

  componentWillMount() {
    this.value = this.props.value;
  }

  render() {
    const size = this.props.size || 16;
    const dotSize = this.props.dotSize || 4;
    const dotMargin = this.props.dotMargin || 2;
    return (
      <div
        onWheel={this.onWheel}
        className={"volume knob"}
        style={{
          display:"inline-block",
          varticalAlign:"middle",
          background: "white",
          borderRadius: "50%",
          position: "relative",
          width: size,
          height: size,
          transform: `RotateZ(${this.angle}deg)`
        }}>
        <div style={{
          background: this.isOn ? "#080" : "#333",
          boxShadow: this.isOn ? "0 0 2px #0f0" : "none",
          borderRadius: "50%",
          position: "absolute",
          top: dotMargin,
          left: (size - dotSize) / 2,
          width: dotSize,
          height: dotSize
        }} />
      </div>
    )
  }
}