import React from "react";
import { observable, computed, action, reaction, get } from "mobx";
import { observer } from "mobx-react";
import {ContextMenuProvider} from "~/components/desktop/ContextMenu"

@observer
export class ListView extends React.Component {
  @observable style="list";
  
  willMountComponent() {
    this.style = this.props.style || "list";
  }

  @computed get filteredItems() {
    return this.props.items || [];
  }

  @computed get sortField() {
    return "name"
  }

  @computed get sortedItems() {
    const f = this.sortField;
    return this.filteredItems.concat().sort((a, b) => +(a[f] > b[f]) || -(a[f] < b[f]) || 0);
  }

  @computed get items() {
    return this.sortedItems;
  }

  @computed get detailFields() {
    return Object.entries(this.props.detailFields || {}).map(([key, { label, classes, format, hidden }]) => ({
      key,
      label: label === null ? "" : (label || key),
      classes: classes || "",
      format: format || (x => x),
      hidden: hidden || false
    }))
  }

  componentWillMount() {
    this.style = this.props.style;
  }

  getMenuOptions = () => {
    return [{
      label: 'View As List',
      execute: ()=>this.style="list"
    },{
      label: 'View As Icons',
      execute: ()=>this.style="icons"
    },{
      label: 'View As Items',
      execute: ()=>this.style="items"
    }]
  }

  @observable currentItemKey = null;

  render() {
    const { idField = "id", extraField="extra" } = this.props;

    return (
      <ContextMenuProvider getOptions={this.getMenuOptions}>
        <div className={"sh listview"} data-style={this.style}>
          <div className="header">
            <span className="icon"></span>
            <span className="name">{this.props.nameLabel || "Name"}</span>
            <span className="extra">{this.props.extraLabel}</span>
            {
              this.detailFields.map(({ key, label, classes }) => (
                <span key={key} className={"details " + classes}>{label}</span>
              ))
            }
          </div>
          <div className="items">
            {
              this.sortedItems.map((item,idx) => {
                const key = item[idField] || idx;
                return (
                  <ListItem
                    key={key}
                    item={item}
                    extraField={extraField}
                    detailFields={this.detailFields}
                    selected={key === this.currentItemKey}
                    onDoubleClick={this.props.onItemDoubleClick}
                    onClick={this.props.onItemClick}
                  />
                );
              })
            }
          </div>
        </div>
      </ContextMenuProvider>
    )
  }
}


@observer
export class ListItem extends React.Component {
  @observable renaming = false;
  @observable renamingTo = "";

  @action.bound
  onNameClick() {
    if (this.props.selected) {
      this.renaming = true;
      this.renamingTo = this.props.item.name;
    }

  }
  render() {
    const { item, selected, extraField='extra', detailFields=[] } = this.props;
    return (
      <div
        className={"sh item" + (selected ? " selected" : "")}
        data-style={this.props.style}
        onClickCapture={e => {
          e.preventDefault();
          e.stopPropagation();
          this.props.onClick && this.props.onClick(item)
        }}
        onDoubleClick={()=>{
          e.preventDefault();
          e.stopPropagation();
          this.props.onDoubleClick && this.props.onDoubleClick(item);
        }}
      >
        <span className="icon">
          <span className="image">
            <img src={item.icon}/>
          </span>
        </span>
        <div className="content">
          <span className="name">{item.name}</span>
          <span className="extra">{item[extraField]}</span>
          {
            detailFields.map(({ key, label, classes, format }) => {
              const value = format(item[key]);
              return <span key={key} title={label+": "+value} className={"details " + classes}>{value}</span>
            })
          }
        </div>
      </div>
    )
  }
}

