import React from "react";
import { observable, computed, action, reaction } from "mobx";
import { observer } from "mobx-react";

@observer
export class Toolbar extends React.Component {
  render() {
    const {buttons}=this.props;
    if (!buttons.concat().length) return null;
    return (
      <div className="sh toolbar">
        {buttons.map(({ name, label, execute, icon }) => (
          <span key={name} title={label} className={name + " button"} onClick={execute}>
            <i className={"ion-md-" + icon}></i> <span className="label">{label}</span>
          </span>
        ))}
      </div>
    )
  }
}