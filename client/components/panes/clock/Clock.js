import {toJS, observable,computed,action,runInAction,reaction} from "mobx";
import { ClockPane } from "./ClockPane";
import { ClockIndicator } from "./ClockIndicator";

export const Clock = new class {
  Pane = ClockPane;
  Indicator = ClockIndicator;
  @observable time = new Date();

  constructor() {
    this.interval = setInterval(() => {
      this.time = new Date();
    }, 1000);
  }

  dispose() {
    clearInterval(this.interval);
  }
}
