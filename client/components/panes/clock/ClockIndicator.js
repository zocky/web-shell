import React from "react";
import { observable, computed, action, reaction, get } from "mobx";
import { observer, disposeOnUnmount } from "mobx-react";
import { Clock } from "./Clock"

@observer 
export class ClockIndicator extends React.Component {
  @computed get time() {
    return Clock.time.toLocaleTimeString('sl').substr(0, 5);
  }

  render() {
    return this.time;
  }
}

