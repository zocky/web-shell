import React from "react";
import { observable, computed, action, reaction, get } from "mobx";
import { observer, disposeOnUnmount } from "mobx-react";
import { Clock } from "./Clock";

@observer 
export class ClockPane extends React.Component {
  @computed get time() {
    return Clock.time;
  }

  render() {
    var h = this.time.getHours() * 30 + this.time.getMinutes() * 0.5;
    var m = this.time.getMinutes() * 6;
    var s = this.time.getSeconds() * 6;
    return (
      <>
        <h1>Clock</h1>
        <div className="section">
          <div style={{
            background: "#666",
            display: "inline-block",
            height: 200,
            width: 200,
            borderRadius: "50%",
            position: "relative",
            margin: "20px 50px"
          }}>
            <div style={{
              position: "absolute",
              width: 6,
              height: 75,
              left: 97,
              top: 25,
              background: "white",
              transformOrigin: "3px 75px",
              transform: "rotateZ(" + h + "deg)"
            }} />
            <div style={{
              position: "absolute",
              width: 2,
              height: 95,
              left: 99,
              top: 5,
              background: "white",
              transformOrigin: "1px 95px",
              transform: "rotateZ(" + m + "deg)"
            }} />
            <div style={{
              position: "absolute",
              width: 2,
              height: 95,
              left: 99,
              top: 5,
              background: "#ccc",
              transformOrigin: "1px 95px",
              transform: "rotateZ(" + s + "deg)"
            }} />
          </div>
        </div>
      </>
    );
  }
}