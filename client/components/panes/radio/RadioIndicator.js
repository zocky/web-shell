import React from "react";
import { observable, computed, action, reaction, get } from "mobx";
import { observer, disposeOnUnmount } from "mobx-react";
import { Radio } from "./Radio";
import { Knob } from "~/components/UI";

@observer 
export class RadioIndicator extends React.Component {
  @observable showPane = false;

  x_componentWillMount() {
    const station = this.props.desktop.registry.get("indicator", "radio", "station");
    if (station) Radio.tune(station);
  }

  render() {
    return (
      <>
        <div 
          title={Radio.station && Radio.station.name} 
          className={"current-station" + (Radio.busy ? " animate-pulse" : "")} 
          style={
            { backgroundImage: "url(" + (Radio.station && Radio.station.icon) + ")" }
          } 
        />
        <Knob
          min={0} max={1} step={0.1}
          value={Radio.volume}
          onChange={v => Radio.volume = v}
          onOn={Radio.play}
          onOff={Radio.stop}
        />
      </>
    );
  }
}

