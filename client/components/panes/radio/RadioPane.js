import React from "react";
import { observable, computed, action, reaction, get } from "mobx";
import { observer, disposeOnUnmount } from "mobx-react";
import { Radio } from "./Radio";
import { ShellContext } from "~/components/desktop/ShellContext";
import { ListView, ListItem } from "~/components/UI";

@observer 
export class RadioPane extends React.Component {
  static contextType = ShellContext;

  @computed get radio() {
    return Radio;
  }

  render() {
    return (
      <>
        <h1>Radio</h1>
        {
          Radio.station &&
          <div className="section">
            <h2>
              Now Playing
            </h2>
            <ListItem style="items" item={Radio.station}/>
          </div>
        }
        <RadioSearch></RadioSearch>
      </>
    )
  }
}

@observer
class RadioSearch extends React.Component {
  static contextType = ShellContext;

  @observable searchTerms = ""; 
  @observable found = [];
  @observable busy = false;

  search = async () => {
    this.busy = true;
    var res = await fetch('http://www.radio-browser.info/webservice/json/stations/byname/' + encodeURIComponent(this.searchTerms), {
      headers: {
        "user-agent": "WebShell/0.1"
      },
      mode: 'cors'
    });
    this.found = (await res.json()).slice(0,100).map(
      ({
        name,
        country,
        favicon,
        id
      }) => ({
        id,
        name,
        extra:country,
        icon:favicon,
      })
    )
    this.busy = false;
  }

  onKey = e => {
    if (e.key === 'Enter') this.search();
  }

  render() {
    return (
      <div className={"section search flex-column flex" + (this.busy ? "busy" : "")}>
        <h2>
          Search
        </h2>
        <div className="flex-row">
          <input type="text" className="flex" value={this.searchTerms} onChange={e => this.searchTerms = e.target.value} onKeyPress={this.onKey} />
          <button onClick={this.search}>Search</button>
        </div>
        <ListView 
          items={this.found} 
          style="items"
          onItemClick={item => Radio.tune(item)}
        />
      </div>
    )
  }
}

@observer class RadioStation extends React.Component {
  static contextType = ShellContext;

  render() {
    const { station } = this.props;
    return (
      <div className="radio-station flex-row" onClick={() => {
        //desktop.registry.set('indicator', 'radio', 'station', station);
        Radio.tune(station)
      }}>
        <div className="image" style={{ backgroundImage: 'url(' + station.favicon + ')' }} />
        <div className="content flex-column">
          <div className="title">{station.name}</div>
          <div className="extra flex">
            {station.state && station.state + ', '}{station.country}
          </div>
        </div>
      </div>
    )
  }
}

