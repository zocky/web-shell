import {toJS, observable,computed,action,runInAction,reaction} from "mobx";
import { RadioPane } from "./RadioPane";
import { RadioIndicator } from "./RadioIndicator";
import { Howl } from "howler";

export const Radio = new class {
  Pane = RadioPane;
  Indicator = RadioIndicator;
  @observable station = null;
  @observable playing = false;
  @observable _volume = 0;

  
  @computed 
  get volume() { return this._volume; }
  set volume(val) { this._volume = val; if (this.playing) this.sound.volume(val);}

  @observable url =  "http://msmn.co:8124/stream";
  @observable busy = false;

  @observable stations = [];

  @action.bound
  playStream(url) {
    this.url = url;

  }
  
  @action.bound
  tune = async (station) => {
    this.station = station;
    var res = await fetch("http://www.radio-browser.info/webservice/v2/json/url/"+encodeURIComponent(station.id), {
      headers: {
        "User-Agent": "WebShell/0.1"
      }
    });
    const data = await res.json();
    runInAction(()=>{
      if (data.ok==="false") {
        this.error = true;
        this.volume = 0;
        return;      
      }
      this.url = data.url;
      this.volume = this.volume || 0.5;
      this.play();
    });
  }

  stop = () => {
    this.sound && this.sound.stop();
    this.sound = null;
  }
  
  play = () => {
    if (this.sound) this.stop();
    this.busy = true;
    this.sound = new Howl({
      src: this.url,
      html5: true,
      format: ["mp3", "aac", "wma","webm","ogg"],
      onload: ()=>{
        this.busy = false;
        this.error = false;
        this.sound.volume(this.volume);
        this.sound.play();
      },
      onplay: () => this.playing = true,
      onstop: () => this.playing = false,
      onerror: () => this.error = !(this.playing = false),
      onloaderror: ()=>{
        this.busy = false;
        this.error = true;
        this.volume = 0;
        console.log("error");
      }
    });
  }
};
