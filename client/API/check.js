const check = {
  validate: () => true,
  coerce: x => x
};

const checks = {
};

function defined(val) {
  return val !== null && val!==undefined && !isNaN(val);
}
function first(...args) {
  for (var val of args) if (defined(val)) return val;
  return undefined;
}

function addCheck(name,validateFn,coerceFn) {
  check[name] = checks[name] = function(...args) {
    const validate = validateFn(...args);
    const coerce = coerceFn(...args);
    const self = this;
    return Object.assign({
      validate(val) { return self.validate(val) && validate(val); },
      coerce(val) { return coerce(self.coerce(val)); },
    },checks);
  };
  Object.assign(check,checks);
}

addCheck("string",
  () => val => typeof val === "string",
  () => val => String(first(val,""))
);

addCheck("number",
  () => val => typeof val === "number",
  () => val => first(Number(val),0)
);

addCheck("max",
  max => val => val <= max,
  max => val => Math.min(val,max)
);

addCheck("min",
  min => val => val >= min,
  min => val => Math.max(val,min)
);