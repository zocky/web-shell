export const API = {
  "test": {
    methods: {
      async ping(msg="ping") {
        var result = await this.channel.request("test","ping");
      },
      log(message) {
        return this.channel.send("test","log",{message});
      }
    }
  },
  "file": {
    onload(spec) {
      const formats = {save:{},open:{},new:{}};
      for (var id in spec.formats) {
        const f = spec.formats[id];
        if (f.open) formats.open[id] = Object.assign({},f,{id});
        if (f.save) formats.save[id] = Object.assign({},f,{id});
        if (f.new) formats.new[id] = Object.assign({},f,{id});
      }
      this.fileFormats = formats;
    },
    actions: {
      "new": {
        label: "New",
        icon: "document",
        check(spec) {
          return Object.keys(this.fileFormats.new).length > 0;
        },
        async execute() {
          if (Object.keys(this.fileFormats.new).length === 1) {
            const format = Object.keys(this.fileFormats.new)[0];
            await this.call("file","new",{format});
            this.currentFile = null;
            this.currentFileFormat = format;
          }
        }
      },
      "open": {
        icon: "folder-open",
        label: "Open",
        check(spec) {
          return Object.keys(this.fileFormats.open).length > 0;
        },
        async execute(spec) {
          const res = await this.desktop.openDialog("file-open",{
            path: this.currentFile ? this.currentFile.path.replace(/[/][^/]*$/,"") : null,
            formats:this.fileFormats.open,
          });
          if (!res) return;
          const {file,format,extension} = res;
          var fileInfo = await this.desktop.fs.read(file.path);
          console.log('open',fileInfo);

          await this.call("file","open",{format,content:fileInfo.content,extension,type:file.type});
          this.currentFile = file;
          this.currentFileFormat = format;
        }
      },
      "save": {
        label: "Save",
        icon: "save",
        check(spec) {
          return this.currentFile && !!this.fileFormats.save[this.currentFileFormat];
        },
        async execute(spec) {
          var content = await this.call("file","save",{format:this.currentFileFormat,extension:this.currentFile.extension,type:this.currentFile.type});
          this.currentFile = await this.desktop.fs.write(this.currentFile.path,content);
          this.log("Saved to",this.currentFile.path);
        }
      },
      "saveas": {
        label: "Save As",
        icon: "save",
        check(spec) {
          return Object.keys(this.fileFormats.save).length > 0;
        },
        async execute(spec) {
          const res = await this.desktop.openDialog("file-save",{
            path: this.currentFile ? this.currentFile.path.replace(/[/][^/]*$/,"") : null,
            fileName:this.currentFile && this.currentFile.name,
            formats:this.fileFormats.save,
          });
          if (!res) return;
          const {path,format,extension} = res;
          const content = await this.call("file","save",{format});
          const file = await this.desktop.fs.write(path,content);
          if (spec.formats[format].open || spec.formats[format].new) {
            this.currentFile = file;
            this.currentFileFormat = format;
          }
          this.log("Saved to",path);
        }
      }
    },
    methods: {
      open({format,content,type,extension}) {
        return this.channel.send("file","open",{format,content,type,extension});
      },
      save({format}) {
        return this.channel.request("file","save",{format});
      },
      new({format}) {
        return this.channel.request("file","new",{format});
      },
    }
  },
  "fs-welcome": {
    onload(spec) {
      const url = String(new URL(spec.home,this.realUrl));
      this.desktop.registry.registerFileSystem(url,this.manifest);
      this.url = url;
    },
    methods: {
    }
  },
  "fs-login": {
    onload(spec) {
      this.url = String(new URL(spec.redirect,this.realUrl));
      this.desktop.registry.set("fs-mounted",this.url,true);
    },
    methods: {
    }
  },
  "fs": {
    onload(spec) {
      this.desktop.fs.mount(spec.mount,this);
    },
    methods: {
      ls(path) {
        return this.channel.request("fs","ls",{path});
      },
      read(path) {
        return this.channel.request("fs","read",{path});
      },
      write(path,content) {
        return this.channel.request("fs","write",{path,content});
      },
      mkdir(path) {
        return this.channel.request("fs","read",{path});
      },
    }
  },
};


