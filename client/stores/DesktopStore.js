import { observable, computed, action, reaction } from "mobx";
import { AppWindowStore, InstallerWindowStore, FileSystemWindowStore } from "./AppWindowStore";
import { FileManagerWindowStore } from "./FileManagerWindowStore";
import { FileSystemStore } from "./FileSystemStore";
import { Promise } from "q";

export class DesktopStore {
  @observable activeFrame = "";

  @observable registry = new RegistryStore();

  constructor() {
    this.fs = new FileSystemStore(this);
    reaction(()=>this.activeFrame,wid => {
      wid && this.focusWindow(wid);
    });
    setInterval(()=>{
      var active = document.activeElement;
      if (active && active.tagName=="IFRAME") this.activeFrame = active.getAttribute("data-wid");
      else this.activeFrame =null;
    },100)
  }

  @observable showMainMenu = false;
  @observable dragging = false;

  @observable windows = {};
  @observable sessions = {};


  @observable orderedWindows = []
  @computed get windowZ() {
    var ret = {};
    this.orderedWindows.forEach((wid,i)=>{
      ret[wid]=this.orderedWindows.length-i;
    })
    return (wid)=>ret[wid];
  }

  @computed get topWindow() {
    return this.orderedWindows[0];
  }

  @action.bound
  windowToTop(wid) {
    this.orderedWindows = [wid].concat(this.orderedWindows.filter(otherWid=>otherWid!==wid))
  }

  @action.bound
  windowToBottom(wid) {
    this.orderedWindows = this.orderedWindows.filter(otherWid=>otherWid!==wid).concat(wid);
  }

  @action.bound
  closeWindow(wid) {
    this.windows[wid].close()
  }

  @action.bound
  unregisterWindow(wid) {
    this.orderedWindows = this.orderedWindows.filter(otherWid=>otherWid!==wid);
    delete this.windows[wid]
  }

  @action.bound
  openWindow(Store,args={}) {
    const wid = Math.random().toString(36).substr(2);
    const window = new Store({desktop:this,wid,...args});
    window.x = Object.values(this.windows).length*100 % 1000 + 50;
    window.y = Object.values(this.windows).length*100 % 500 + 50;
    this.windows[wid] = window;
    this.orderedWindows.push(wid);
    return window;
  }

  @action.bound
  installApp(url) {
    if (this.registry.get('apps',url)) return;
    const window = this.openWindow(InstallerWindowStore,{url});
    window.hide();
  }

  @action.bound
  openApp(url) {
    const window = this.openWindow(AppWindowStore,{url});
    window.focus();
    return window;
  }

  @action.bound
  openFileSystem(url) {
    const window = this.openWindow(FileSystemWindowStore,{url});
    window.w=400;
    window.h=300;
    window.focus();
    return window;
  }

  openFileManager(path) {
    const window = this.openWindow(FileManagerWindowStore,{path});
    window.focus();
    return window;
  }

  @action.bound
  focusWindow(wid,skip) {
    var window = this.windows[wid];
    this.windowToTop(wid);
    window.unminimize();
    window.unhide();
    skip || window.iframe && setTimeout(()=>{
      //window.iframe && window.iframe.contentWindow.focus();
    },300)
  }

  blurWindow(wid) {
    this.windowToBottom(wid);
  }



  isActive(wid) {
    return this.topWindow === wid;
  }

  @observable dialog = null;
  openDialog(type,opt) {
    if (this.dialog) {
      throw "double modal dialog"
    }
    return new Promise((resolve,reject) => {
      this.dialog = Object.assign({},opt,{type,close:val => {
        this.dialog = null;
        resolve(val);
      }});
    })
  }
}

class RegistryStore {

  @observable registry = {};

  get(...args) {
    var cur = this.registry;
    const parts = args.join('.').split('.')
    for (var p of parts) {
      cur = cur[p];
      if (!cur) return cur;
    }
    return cur;
  }

  _find(args) {
    const parts = args.concat();
    const key = parts.pop();
    var cur = this.registry;
    for (var p of parts) {
      if (cur.__proto__!== Object.prototype) throw "bad registry path "+parts.join('.');
      if (!(p in cur)) {
        cur[p]={};
      };
      cur = cur[p]
    }
    return {
      obj:cur,
      key,
      value:cur[key]
    }
  }

  set(...args) {
    const value = args.pop();
    const {obj,key} = this._find(args);
    delete obj[key];
    obj[key]=value;
    this.registry = Object.assign({},this.registry);
    this.save();
  }
  
  patch(...args) {
    const changes = args.pop();
    const {obj,key} = this._find(args);
    obj[key]=Object.assign({},obj[key],changes);
    this.registry = Object.assign({},this.registry);
    this.save();
  }
  
  delete(...args) {
    const {obj,key} = this._find(args);
    delete obj[key];
    this.registry = Object.assign({},this.registry);
    this.save();
  }

  push (...args) {
    const val = args.pop();
    if (!obj[key]) obj[key]=[];
    if (!Array.isArray(obj)) throw "Not an array "+args.join(" ");
    obj[key]=[...obj[key],val];
    this.registry = Object.assign({},this.registry);
    this.save();
  }

  ensure(...args) {
    const val = args.pop();
    const {obj,key} = this._find(args);
    if (key in obj) return;
    obj[key]=val;
    this.registry = Object.assign({},this.registry);
    this.save();
  }


  constructor () {
    this.load();
    this.ensure('apps',{})
    this.ensure('fs',{})
    this.ensure('fs-mounted',{})
  }

  load() {
    try {
      this.registry = JSON.parse(localStorage.getItem('desktop_registry')) || {};
    } catch (error) {
    }
  }

  save() {
    localStorage.setItem('desktop_registry',JSON.stringify(this.registry));
  }

  registerApp(url,manifest,state={},settings={}) {
    this.set('apps',url,{url,manifest,state,settings});
  }
  registerFileSystem(url,manifest,state={},settings={}) {
    console.log('register fsss',url)
    this.set('fs',url,{url,manifest,state,settings});
  }

  unregisterApp(url) {
    this.delete('apps',url);
  }

  changeAppState(url,changes={}) {
    this.patch('apps',url,'state',changes);
  }
  changeAppSettings(url,changes={}) {
    this.patch('apps',url,'settings',changes);
  }
}