import { observable, computed, action, reaction } from "mobx";

export class FileSystemStore {
  @observable mtab = {}

  @action.bound
  mount(id, app) {
    this.mtab[id] = { app, id };
  }

  @action.bound
  umount(id) {
    delete this.mtab[id];
  }

  @computed get root() {
    return Object.values(this.mtab).map(m => {
      return {
        name: m.app.title,
        path: "/" + m.id,
        type: "folder/remote",
        is: {
          folder: true
        }
      };
    });
  }

  async ls(path) {
    if (path.split("/").filter(Boolean).length === 0) {
      return this.root;
    } else {
      const { id, app, p } = this.parsePath(path);
      const files = await app.call("fs", "ls", p);
      return files.map(file => new FileSystemFile(fixPath(id, p, file.name), file));
    }
  }

  async read(path) {
    const { id, app, p } = this.parsePath(path);
    const content = await app.call("fs", "read", p);
    return content;
  }

  async write(path, content) {
    const { id, app, p } = this.parsePath(path);
    var file = await app.call("fs", "write", p, content);
    file.path = fixPath(id, p);
    return file;
  }

  async mkdir(path) {
    const { id, app, p } = this.parsePath(path);
    const content = await app.call("fs", "mkdir", p);
    file.path = fixPath(id, p);
    return content;
  }

  parsePath(path) {
    const parts = path.split("/").filter(Boolean);
    const [id, p] = [parts.shift(), "/" + parts.join("/")];

    if (!id) throw "bad-path";
    if (!this.mtab[id]) throw "bad-path";
    return {
      id,
      p,
      app: this.mtab[id].app
    };
  }

  matchFile(file,f) {
    return matchFile(file,f);
  }
}


function fixPath(...args) {
  return "/" + args.join("/").split("/").filter(Boolean).join("/");
}

class FileSystemFile {
  constructor(path, { id, name, type, size, is, atime, ctime, mtime }) {
    Object.assign(this, { id, path, name, type, size, is, ctime, mtime, atime });
  }
  match(f)  {
    return matchFile(this,f);
  }
}

function matchFile(file, { extension = "*", type = "*/*" }={}) {
  if (extension && !extension.split(/\s+/).find(e => e === "*" || file.name.endsWith("." + e))) return false;
  if (type && file.type) {
    const [main, sub] = type.split("/");
    if (main !== "*" && !file.type.startsWith(main + "/")) return false;
    if (sub !== "*" && !file.type.endsWith("/" + sub)) return false;
  }
  return true;
}
 