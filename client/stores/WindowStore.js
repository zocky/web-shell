import { observable, computed, action, reaction } from "mobx";

export class WindowStore {
  wid = null;
  @observable group = null;

  @observable is = {
    maximized: false,
    minimized: false,
    hidden:false
  }


  @observable icon = null;
  @observable title = "" ;
  @observable x=0;
  @observable y=0;
  @observable w=800;
  @observable h=600;
  @observable.shallow logs = []

  @computed 
  get toolbarButtons() {
    return []
  }

  @computed 
  get titlebarButtons() {
    const buttons = [];
    buttons.push({name:'close',icon:'close',execute:this.close});
    buttons.push({name:'minimize',icon:'remove',execute:this.minimize});
    if (!this.is.maximized) {
      buttons.push({name:'maximize',icon:'expand',execute:this.maximize});
    } else {
      buttons.push({name:'unmaximize',icon:'contract',execute:this.unmaximize});
    }
    return buttons;
  }

  @computed get z(){
    return this.desktop.windowZ(this.wid);
  }

  @action.bound
  _log(type,message) {
    var l = {type,message,dismiss:()=>{
      this.logs = this.logs.filter(ll=>ll!==l);
      clearTimeout(t);
    }};
    this.logs.push(l);
    var t=setTimeout(l.dismiss,3000)
  }
  log(...args) {
    this._log('message',args.join(' '));
  }
  error(error) {
    this._log('error',error.message || String(error))
  }

  constructor ({desktop,wid}) {
    this.desktop = desktop;
    this.wid = wid;
  }

  @computed get isActive() {
    return !this.is.minimized && this.desktop.isActive(this.wid);
  }

  @action.bound
  hide() {
    this.is.hidden = true;
  }

  @action.bound
  unhide() {
    this.is.hidden = false;
  }

  @action.bound
  maximize() {
    this.is.maximized = true;
  }

  @action.bound
  unmaximize() {
    this.is.maximized = false;
  }

  @action.bound
  minimize() {
    this.blur();
    this.is.minimized = true;
  }

  @action.bound
  unminimize() {
    this.is.minimized = false;
  }

  @action.bound
  close() {
    this.desktop.unregisterWindow(this.wid);
  }

  @action.bound
  focus(skip) {
    this.desktop.focusWindow(this.wid,skip);
  }

  @action.bound
  blur(skip) {
    this.desktop.blurWindow(this.wid,skip);
  }

  @action.bound
  move(dx,dy) {
    this.x+=dx;
    this.y+=dy;
  }

  @action.bound
  resize(dw,dh) {
    this.w = Math.max(this.w+dw,200);
    this.h = Math.max(this.h+dh,100)
  }
}
