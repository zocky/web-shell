import { observable, computed, action, reaction, get } from "mobx";
import { WindowStore } from "./WindowStore.js";

export class FileManagerWindowStore extends WindowStore {
  windowType = 'filemanager';
  group = "filemanager";

  @observable icon="/assets/icons/Humanity/places/64/gnome-folder.svg"

  @computed get description() {
    return "Browsing files";
  }
}
