import { observable, computed, action, reaction, get } from "mobx";
import { WindowStore } from "./WindowStore.js";
import { Channel } from "../API/Channel.js";
import { API } from "../API";

export class RemoteWindowStore extends WindowStore {
  @observable appState = 'loading';

  @observable url;
  @observable.shallow manifest;

  @computed get group() {
    return this.url;
  }

  @computed get icon() {
    return this.manifest && this.manifest.icon;
  }

  @computed get api () {
    const apis = { test:API.test };
    if (!this.manifest) return apis;
    for (var id in this.manifest.api) {
      if (API[id]) {
        apis[id] = API[id];
      } else {
        console.error('app reports bad API',id);
      }
    }
    return apis;
  };

  @computed
  get toolbarButtons() {
    const buttons = [];
    for (let id in this.api) {
      const api = this.api[id];
      for (let a in api.actions) {
        const action = api.actions[a];
        if (!action.check || action.check.call(this,this.manifest.api[id])) {
          buttons.push({name:a,icon:action.icon,label:action.label,execute: async() => {
            this.action(id,a);
          }})
        }
      }
    }
    return buttons;
  }


  register() {
  }

  loaded = async e => {
    this.channel = await Channel.connect(this.iframe.contentWindow);
    this.manifest = this.channel.manifest;
    this.manifest.icon = new URL(this.manifest.icon,this.realUrl)
    this.register();
    for (var id in this.api) {
      this.api[id].onload && this.api[id].onload.call(this,this.manifest.api[id]);
    }
    this.appState = "ready";
  }

  @action.bound
  async call(api,cmd,...args) {
    try {
      if (!this.api[api]) throw this.wid + " bad API "+api;
      if (!this.api[api].methods[cmd]) throw this.wid + " bad cmd "+api+" "+cmd;
      const fn = this.api[api].methods[cmd];
      var result = await fn.apply(this,args);
      return result;
    } catch (error) {
      console.log(error);
      this.error(error);
    }
  }
  
  @action.bound
  async action(api,a) {
    try {
      const myApi = this.api[api];
      if (!myApi) throw this.wid + " bad API "+api;
      const fn = myApi.actions[a];
      if (!fn) throw this.wid + " bad action "+api+" "+a;
      var result = await fn.execute.call(this,this.manifest.api[api]);
      return result;
    } catch (error) {
      console.log(error);
      this.error(error);
    }
  }

  @computed get realUrl() {
    return this.url;
  }

  @computed get title() {
    var title = this.manifest ? this.manifest.title : this.url;
    return title;
  }

  @computed get description() {
  }

  
  constructor ({url,...rest}) {
    super(rest);
    this.url = url;
  }
}


export class AppWindowStore extends RemoteWindowStore {
  windowType = 'app';
  @observable currentFile = null;
  @observable currentFileFormat = null;
  @observable fileFormats = {new:{},save:{},open:{}}

  @computed get realUrl() {
    if (this.url.match(/^[a-z0-9-]+$/)) {
      return location.protocol+'//app-'+this.url+'.'+ process.env.DOMAIN + ':' + process.env.PORT;
    }
    return this.url;
  }

  @computed get title() {
    var title = this.manifest ? this.manifest.title : this.url;
    if (this.currentFile) title += " | " +this.currentFile.name;
    return title;
  }

  @computed get description() {
    return JSON.stringify(this.currentFile,null,2)
  }
  register() {
    this.desktop.registry.registerApp(this.url,this.manifest);
  }
}

export class InstallerWindowStore extends AppWindowStore {
  register() {
    this.desktop.registry.registerApp(this.url,this.manifest);
    this.close();
  }
}


export class FileSystemWindowStore extends RemoteWindowStore {

  windowType = 'fs';

  @computed get realUrl() {
    if (this.url.match(/^[a-z0-9-]+$/)) {
      return location.protocol+'//fs-'+this.url+'.'+ process.env.DOMAIN + ':' + process.env.PORT;
    }
    return this.url;
  }

  @action.bound
  close() {
    if (!this.api['fs-login'] && !this.api['fs']) return super.close();
    this.hide()
  }

  register() {
  }
}
