const textEncoder = new TextEncoder();
const textDecoder = new TextDecoder();

export async function recode(input,from,to) {
  const buffer = await toBuffer[from](input);
  return await fromBuffer[to](buffer);
}

const fromBuffer = {
  binary(buffer) {
    return buffer;
  },
  text(buffer) {
    return textDecoder.decode(buffer);
  },
  json(buffer) {
    const text = textDecoder.decode(buffer);
    return JSON.parse(text);
  },
  async base64(buffer) {
    return new Promise(resolve=>{
      const blob = new Blob([buffer], {type:"application/octet-binary"});    
      var fileReader = new FileReader();
      fileReader.onload = function() {
        var dataUrl = fileReader.result;
        resolve(dataUrl.substr(dataUrl.indexOf(",")+1));
      };
      fileReader.readAsDataURL(blob);
    });
  }
};

const toBuffer = {
  binary(buffer) {
    return buffer;
  },
  text(string) {
    return textEncoder.encode(string);
  },
  json(obj) {
    const text = JSON.stringify(obj);
    return textEncoder.encode(text);
  },
  async base64(string) {
    const dataUrl = "data:application/octet-binary;base64," + string;
    const res = await fetch(dataUrl);
    const buffer = await res.arrayBuffer();
    return new Uint8Array(buffer);
  }
};
