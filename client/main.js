import React from "react";
import { render } from "react-dom";
import { DesktopStore } from "./stores"
import { Desktop } from './components/desktop/Desktop';
import "./style/desktop.less";

const DESKTOP = new DesktopStore();

DESKTOP.installApp("textarea");
DESKTOP.installApp("ace");
DESKTOP.installApp("quill");
DESKTOP.installApp("sketch");

const mounted = (DESKTOP.registry.get('fs-mounted'));
if (mounted && Object.keys(mounted).length>0) {
  for (var url in mounted) {
    DESKTOP.openFileSystem(url).hide();
  }
} else {
  DESKTOP.openFileSystem("host");
}
//DESKTOP.openFileSystem("google");

setTimeout(()=>{
  DESKTOP.openFileManager("/zocky@fs-host.127-0-0-1.sslip.io")
},500)

render(
  <Desktop desktop={DESKTOP}/>,
  document.getElementById("root")
);