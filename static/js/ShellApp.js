window.ShellApp = (function () {
  var port = null;
  var manifest = {};
  const handlers = {};

  ShellApp = {
    on(api,cmd,fn) {
      handlers[api+'|'+cmd] = fn;
    },
    manifest(m) {
      manifest = m;
    },
    trigger(event,data={}) {
      port.postMessage({event,data});
    }
  }; 


  async function receive({api,cmd,args,rsvp}) {
    if (api && cmd && handlers[api+'|'+cmd]) {
      try {
        var result = await handlers[api+'|'+cmd](args);
        port.postMessage({result,re:rsvp})
      } catch (error) {
        port.postMessage({error:error.message||String(error),re:rsvp})
      }
    };
  }
  
  function connect(ev) {
    if (ev.source !== window.parent) return;
    if (!ev.data || !ev.data.port || !ev.data['SOUTH-TOOTH']==='connect') return;
    port = ev.data.port;
    port.postMessage({'SOUTH-TOOTH':'connected',manifest})
    window.removeEventListener('message',connect);
    port.onmessage = ev => {
      receive(ev.data);
    }
  }
  window.addEventListener('message',connect);
  
  ShellApp.on('test','ping',()=>{
    console.log('PING');
    return 'pong'
  });
  ShellApp.on('test','log',({message})=>{console.log(message)})

  return ShellApp;
})()
