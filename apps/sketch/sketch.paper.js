
var hitOptions = {
    segments: true,
    stroke: true,
    fill: true,
    tolerance: 4,
};



var selectionRect = null;
var spanRect = null;
var selection = {};
var selectionCount = 0;
var handleMode = 'size';

var clickMode = 'select';
var dragMode = null;
var dragObject = null;
var dragged = false;
var gridLayer = new Layer();
var mainLayer = new Layer();
var activeItem = mainLayer;
var selectionLayer = new Layer();
var activeBoundaryLayer = new Layer();
var segmentLayer = new Layer();
var spanLayer = new Layer();
var guideLayer = new Layer();
var rulerLayer = new Layer();
var toolTextLayer = new Layer();
var libraryLayer = new Layer();




//***************************************************************************************************************************
//                                        
//     S E L E C T I O N  L A Y E R
//                                        
//***************************************************************************************************************************


function addModeButtons() {
  var modeButtonCount = 0;
  function makeModeButton(mode) {
    var y = selectionRect.y - 10/project.view.zoom + modeButtonCount * 30/project.view.zoom;
    var x = selectionRect.x - 45/project.view.zoom ;
    modeButtonCount++;
    var path = new Path.RoundRectangle(new Rectangle(x,y,25/project.view.zoom,25/project.view.zoom),3/project.view.zoom);
    path.style = {
      strokeColor:'blue',
      strokeWidth:1/project.view.zoom,
      fillColor: mode == handleMode ? '#88f' : 'white'
    }
    var t = new PointText();
    t.content = handleModes[mode].char;
    t.style = {
      font:'Trebuchet MS, Arial',
      fontSize:18/project.view.zoom,
      fillColor: mode == handleMode ? 'white' : 'blue'
    };
    t.position = path.position;
    path.handleMode = t.handleMode = mode;
    selectionLayer.addChild(path);
    selectionLayer.addChild(t);
  }
  modeButtonCount = 0;
  for (var i in handleModes) {
    if (!handleModes[i].check || handleModes[i].check()) makeModeButton(i);
  }
}


function drawSelection(m) {

  selectionLayer.removeChildren();
  activeBoundaryLayer.removeChildren();

  if (activeItem != mainLayer) {
    var s = Path.Rectangle(getBounds(activeItem));
    s = offsetPath(s,-1050/project.view.zoom,true);
    s.style = {
      strokeColor:new RGBColor(0.3,0.3,0.9,0.1),
      strokeWidth:2000/project.view.zoom,
    };
    activeBoundaryLayer.addChild(s);
  }
  
  if (!selectionRect) return;

  var stroke = selectedSegments.length ? 'silver' : 'blue';
  var fill = selectedSegments.length ? 'silver' : '#88f';

  var rect = new Rectangle(selectionRect);
  var p = new Path();
  p.moveTo(rect.x,rect.y);
  p.lineBy(rect.width/2,0);
  p.lineBy(rect.width/2,0);
  p.lineBy(0,rect.height/2);
  p.lineBy(0,rect.height/2);
  p.lineBy(-rect.width/2,0);
  p.lineBy(-rect.width/2,0);
  p.lineBy(0,-rect.height/2);
  p.closePath();
  p.style = {
    strokeColor:stroke,
    strokeWidth:1/project.view.zoom,
    fillColor: new RgbColor(1,1,1,0.001)
  };
  if(m) p.transform(m);
  
  p = offsetPath(p,-10/project.view.zoom,true);
  p2 = p.clone();
  p2.style = {
    strokeColor:'white',
    strokeWidth:2/project.view.zoom,
  };
  
  selectionLayer.addChild(p2);
  selectionLayer.addChild(p);

  if (selectionCount > 1 && focusedItem) {
    var s = Path.Rectangle(focusedItem.bounds);
    s = offsetPath(s,-5/project.view.zoom,true);
    s.style = {
      strokeColor:'#88f',
      strokeWidth:1/project.view.zoom,
      fillColor: new RgbColor(1,1,1,0.001)
    };
    selectionLayer.addChild(s);
  }



  selectionRect.handles={};
  function makeHandle(n,which,ox,oy) {
    var s = p.segments[n];
    var path = handleModes[handleMode].drawHandle(which,s,ox,oy);
    path.style = {
      strokeColor:stroke,
      strokeWidth:1/project.view.zoom,
      fillColor: handleModes[handleMode].fill ? fill : 'white'
    }
    path.scale(1/project.view.zoom);
    path.whichHandle = which;
    selectionLayer.addChild(path);
  }
  if (handleModes[handleMode].drawHandles) handleModes[handleMode].drawHandles();
  else {
    makeHandle(0,'topLeft',-1,-1);
    makeHandle(1,'topCenter',0,-1);
    makeHandle(2,'topRight',1,-1);
    makeHandle(3,'rightCenter',1,0);
    makeHandle(4,'bottomRight',1,1);
    makeHandle(5,'bottomCenter',0,1);
    makeHandle(6,'bottomLeft',-1,1);
    makeHandle(7,'leftCenter',-1,0);
  }
  drawSelectedSegments();
  return p;
}
function drawSelectedSegments() {
  return;
  segmentLayer.removeChildren();
  for (var i=0; i<selectedSegments.length; i++) {
    var s = Path.RegularPolygon(selectedSegments[i].point,4,6/project.view.zoom);
    s.style = {
      strokeColor:'blue',
      strokeWidth:1/project.view.zoom,
      fillColor:'#88f'
    }
    segmentLayer.addChild(s);
  }
}
function clearToolText () {
    toolTextLayer.removeChildren();
}
function drawToolText (point, text) {
    toolTextLayer.removeChildren();
    var t = new PointText(point);
    t.content = text;
    t.characterStyle = {
      fillColor: 'white',
      fontSize: 12,
      font: 'Trebuchet MS, Arial',
      strokeWidth: 0,
    }
    t.paragraphStyle._justification = 'center';
    
    var rect = new Rectangle(t.bounds);
    rect.point.x -= 5;
    rect.width += 10;
    rect.point.y -= 2;
    rect.height += 4;
     
    var r = Path.RoundRectangle(rect,3);
    r.style = {
      fillColor: new RGBColor(0,0,0,0.5),
      strokeWidth: 0.5/project.view.zoom,
      strokeColor: 'white'
    }
    t.scale(1/project.view.zoom);
    r.scale(1/project.view.zoom);
    var g = new Group();
    g.addChild(r);
    g.addChild(t);
    toolTextLayer.addChild(g);
    return g;
}

function showSelection() {
  selectionLayer.removeChildren();
  var rect;
  var data = {};
  for (var i in selection) {
    var s = selection[i];
    for (var j in itemProps[s.data.kind].defaults) {
      var val = getProp(s,j);
      if (!(j in data)) data[j] = val;
      else if (data[j]!=val) data[j] = null;
    }
    rect = rect ? rect.unite(getBounds(s)) : new Rectangle(getBounds(s));
  }
  selectionRect = rect;
  drawSelection();
  if (rect) {
    addModeButtons();
    selectionLayer.addChild(drawToolText(rect.topCenter - 25/project.view.zoom, focusedItem && selectionCount == 1 ? focusedItem.data.kind : selectionCount + ' items'));
  }

  $('.data').each(function() {
    var $this = $(this);
    var prop = $this.attr('prop');
    if (!(prop in data)) {
      $this.closest('dd').prev().addBack().css('display','none');
      return;
    }
    $this.closest('dd').prev().addBack().css('display','inline-block');
    var val = data[prop];
    if ($this.hasClass('color')) {
      if (!val) {
        $this.addClass('multiple');
      } else {
        $this.removeClass('multiple');
        $this.val(colorToHex(project.usedColors[val].color));
      }
    } else {
      $this.val(val===null ? 'multiple' : val);
    }
  });
  
  $('#fillColor').val(data.fill && data.fill.hex);
  $('#strokeColor').val(data.stroke && data.stroke.hex);
  
  $('.swatch').removeClass('fill-selected fill2-selected stroke-selected');
  
  if (data.fill) $('.swatch[swatch-name="'+data.fill+'"]').addClass('fill-selected');
  if (data.fill2) $('.swatch[swatch-name="'+data.fill2+'"]').addClass('fill2-selected');
  if (data.stroke) $('.swatch[swatch-name="'+data.stroke+'"]').addClass('stroke-selected');
  
  $('.action').each(function () {
    var $this = $(this);
    var a = actions[$this.attr('action')]
    if (!a) $this.css('color','red');
    if (a && (!a.check || a.check())) $this.removeAttr('disabled');
    else $this.attr('disabled','disabled');
  });
  updateExplorer();
}


//***************************************************************************************************************************
//                                        
//     S E L E C T I O N 
//                                        
//***************************************************************************************************************************

//***************************************************************************************************************************
// I T E M S

var focusedItem = null;
var segmentItem = null;

var setFocus = function(item1, item2) {
  if (segmentItem) {
    //segmentItem.selected = false;
    segmentItem.locked = false;
  }
  focusedItem = item1;
  var s = item2 === undefined ? item1 : item2;
  
  if (s && s.data && s.data.kind == 'path') {
    segmentItem = s;
    //segmentItem.selected = true;
    segmentItem.locked = true;
  }
}

var focusItem = function(item) {
  blurItem();
  (itemProps[item.data.kind].focus || itemProps._.focus)(item)
};
var blurItem = function(item) {
  deselectAllSegments();
  segmentItem = null;
  if (item && item != focusedItem) return;
  if (focusedItem) {
    (itemProps[focusedItem.data.kind].blur || itemProps._.blur)(focusedItem);
  }
};

function selectItem(item) {
  if (!item.isParent(activeItem)) return;
  if (!selection[item.id]) selectionCount++;
  selection[item.id] = item;
  focusItem(item);
}
function deselectItem(item) {
  if (!selection[item.id]) return;
  blurItem(item);
  delete selection[item.id];
  selectionCount--;
  if (selectionCount) { for(var i in selection); focusItem(selection[i]); }
}
function deselectAll() {
  for (var i in selection) deselectItem(selection[i]);
  handleMode = 'size';
}
function isSelected(item) {
  return item.isParent(activeItem) && selection[item.id];
}
function toggleItem(item) {
  if (isSelected(item)) deselectItem(item);
  else selectItem(item);
}

//***************************************************************************************************************************
// S E L E C T I O N   C L O N E 

var segmentItemClone = null;
var selectionClone = null;
var selectionCloneMethod = null;

function initSelectionClone() {
  selectionCloneMatrix = new Matrix();
  segmentLayer.visible=false;
  for (var i in selection) {
    selection[i].visible=false;
    selection[i].selected=false;
  }
  transformSelectionClone();
}

function applySelectionClone() {
  if (!selectionClone) return;
  for (var i in selection) {
    selectionClone[i].remove();
    selection[i].visible = true;
    selectionCloneMethod(selection[i]);
  }
  segmentLayer.visible=true;
  showSelection();
  selectionClone = null;
  selectionCloneMethod = null;
  segmentItemClone = null;
}

function transformSelectionClone(fn,text) {

  for (var i in selectionClone) selectionClone[i].remove();
  selectionClone = {};
  for(var i in selection) {
    var s = selection[i];
    var c = selectionClone[i] = s.clone();
    c.remove();
    s.parent.insertChild(s.index,c);
    c.visible=true;
    if (selection[i]==segmentItem) segmentItemClone = c;
  }
  if (!fn) return;
  selectionCloneMethod = fn;
//  if (mode) for (var i in selectionClone) selectionClone[i].position = selectionClone[i].position.transform(m);
//  else for (var i in selectionClone) { selectionClone[i].transform(m); selectionClone[i].apply() };
  for (var i in selectionClone) fn(selectionClone[i],i);
}
function fixSelection() {
  var c = activeItem._getClipItem();
  if (c) {
    c.remove();
    activeItem.insertChild(0,c);
  }
  return;
}

//***************************************************************************************************************************
// S E G M E N T  S E L E C T I O N

selectedSegments = [];
function selectSegment(s) {
  if (isSegmentSelected(s)) return;
  selectedSegments.push(s);
  //showSelection();
  s.selected = true;
}
function deselectSegment(s) {
  var n = selectedSegmentIndex(s);
  if (n<0) return;
  selectedSegments.splice(n,1);
  s.selected = false;
  //drawSelection();
}
function deselectAllSegments(s) {
//  segmentItem.selected = false;

  while(selectedSegments.length) selectedSegments.pop().selected=false;
  if (segmentItem) segmentItem.selected = false;
  //drawSelection();
}

function selectedSegmentIndex(s) {
  return selectedSegments.indexOf(s);
}

function isSegmentSelected(s) {
  return selectedSegments.indexOf(s)>-1;
}

//***************************************************************************************************************************
//                                        
//    M O U S E   E V E N T S
//                                        
//***************************************************************************************************************************


function setClickMode(mode) {
  clickMode = mode;
//  $('#myCanvas').css('cursor', mode == 'select' ? 'default' : 'crosshair');

  $('button.tool').removeClass('selected');
  $('button.tool[tool="'+mode+'"]').addClass('selected');
}


var lastMouseDown;
function onMouseDown(event) {
  $(':focus').blur();
  event.modifiers.shift = event.event.shiftKey;
  event.modifiers.control = event.event.ctrlKey;
  
  var now = new Date().valueOf();
  var t =  now - lastMouseDown;
  var doubleclick = t<250;
  var insertObject = null;
  lastMouseDown = now;
 
  event.preventDefault();
  dragMode = null;
  dragged = false;
  
  if (event.event.button==1) {
    event.event.preventDefault();
    dragMode = 'pan';
    dragObject = event.point - project.view.center;
    project.view.draw();
    return;
  }
  
  
  switch (clickMode) {
  case 'draw':
    var p = new Path();
    p.strokeColor = 'black';
    p.add(event.point);
    activeItem.addChild(p);
    dragMode = 'draw';
    dragObject = p;
    return;
  case 'text':
    deselectAll();
    var t = new PointText(event.point);
    t.characterStyle._fontSize = 12;
    blessItem(t,'text');
    activeItem.addChild(t);
    setClickMode('select');
    selectItem(t);
    $('#text').focus().select();
    return;
  case 'circle':
    insertObject = convertPath(Path.Circle(snapPoint(event.point+new Point(0.5,0.5)),'0.5'),{smoothness:1});
    
  case 'rectangle':
    insertObject = insertObject || convertPath(Path.Rectangle(new Rectangle(snapPoint(event.point),1)),{smoothness:0});
    activeItem.addChild(insertObject);
    handleMode = 'size';
    deselectAll();
    selectItem(insertObject);
    showSelection();
    initHandleDrag('size','bottomRight');
    setClickMode('select');
    return;
  case 'curve':
  case 'lines':
    deselectAll();
    var p = new Path();
    convertPath(p);
    setProp(p,'smoothness',clickMode == 'lines' ? 0 : 1);
    var s = addSegment(p,snapPoint(event.point));
    dragObject = addSegment(p,snapPoint(event.point));
    activeItem.addChild(p);
    project.view.draw();
    dragMode = clickMode = 'continuecurve';
    break;
  case 'continuecurve':
    var done = false;
    var p = dragObject.path;
    project.view.draw();
    if (doubleclick) {
      if (event.modifiers.shift || snapPoint(event.point).getDistance(p.segments[0].point)<5/project.view.zoom) {
        p.removeSegments(dragObject.index-1,dragObject.index+1);
        p.closePath();
        smoothPath(p);
      } else {
        p.removeSegments(dragObject.index-1,dragObject.index);
      }
      setClickMode('select');
      selectItem(p);
      dragMode=null;
      undoPush();    
    } else {
      dragObject = addSegment(dragObject.path,snapPoint(event.point));
    }
    break;    
  case 'select':
    if (UI.showGuides) {
      var hit = topRulerRaster.hitTest(event.point);
      if (hit) {
        dragMode = 'newhorizontalguide';
        dragObject = null;
        return;
      }
      var hit = leftRulerRaster.hitTest(event.point);
      if (hit) {
        dragMode = 'newverticalguide';
        dragObject = null;
        return;
      }
    }
    if (segmentItem && handleModes[handleMode].editSegments) {
      var hit = segmentItem.hitTest(event.point, {
        segments: true,
        stroke: true,
        fill: true,
        tolerance: 5/project.view.zoom,
      });
      if (hit && hit.item == segmentItem) {
        var item = hit.item;
        switch (hit.type) {
        case 'segment':
//           hit.segment.selected = true;
          if (event.modifiers.shift) {
            selectSegment(hit.segment);
            dragObject = hit.segment;
            dragMode = 'segment';
//            hit.segment.sharp = !hit.segment.sharp;
//            smoothPath(hit.segment.path);
          } else if (event.modifiers.control) {
              deselectSegment(hit.segment);
              dragMode = null;
//            removeSegment(hit.segment);
//            smoothPath(item);
          } else {
            if(!isSegmentSelected(hit.segment)) deselectAllSegments();
            selectSegment(hit.segment);
            dragObject = hit.segment;
            dragMode = 'segment';
          }
          return;
        case 'stroke': 
          if (event.modifiers.shift) {
            dragObject =  addSegment(item,event.point,hit.location.index + 1);
            deselectAllSegments();
            selectSegment(dragObject);
            undoPush();
            dragMode = 'segment';
          } 
          return;
        }
      }
    }

    var hit = selectionLayer.hitTest(event.point, {
      fill: true,
      tolerance: 1
    });
    if (hit) {
      if (hit.item.whichHandle) {
// HANDLES      
  
        initHandleDrag(handleMode,hit.item);
        
        return;
      } else if (hit.item.handleMode) {
        handleMode = hit.item.handleMode;
        showSelection();
        return;
      } else if (hit.item.action) {
//        actions[hit.item.action].method();
        return;
      } else {
        if (doubleclick) {
          doAction('openItem');
          return;
        }
        dragObject = event.point - selectionRect.topLeft;
        dragMode = 'selection';
      }
    }

    var hit = activeItem.hitTest(event.point, {
      segments: true,
      stroke: true,
      fill: true,
      tolerance: 5/project.view.zoom
    });
    if (!hit) {
    
      if (UI.showGuides) {
        var p = snapToGuide('y',event.point,2);
        if (p.snapped) {
          dragObject = horizontalGuideRaster.clone();
          dragObject.position = new Point(project.view.center.x,p.y);
          spanLayer.addChild(dragObject);
          dragMode = 'movehorizontalguide';
          dragObject.originalPosition = p.y;
          removeGuide('y',p.y);
          drawGrid();
          return;
        } 
        var p = snapToGuide('x',event.point,2);
        if (p.snapped) {
          dragObject = verticalGuideRaster.clone();
          dragObject.position = new Point(p.x,project.view.center.y);
          spanLayer.addChild(dragObject);
          dragMode = 'moveverticalguide';
          dragObject.originalPosition = p.x;
          removeGuide('x',p.x);
          drawGrid();
          return;
        } 
      }


      if(doubleclick) {
        doAction('closeItem');
        return;
      };
      if (dragMode == 'selection') {
        deselectAllSegments();
        return;
      }
      if (event.event.button==0) {
        //deselectAll();
        dragMode = 'span';
        dragObject = new Point(event.point);
      }  
      return;
    }
    var item = hit && hit.item;
    while (item && item.parent != activeItem) item = item.parent;
    if (event.modifiers.shift) {
      selectItem(item);
      showSelection();
      dragObject = event.point - selectionRect.topLeft;
      dragMode = 'selection';
      return;
    } else if (event.modifiers.control) { 
      deselectItem(item);
      dragMode = null;
    } else  {
      if (!isSelected(item)) {
        deselectAll();
        dragMode = 'item';
      } else {
        dragMode = 'selection';
      }
      selectItem(item);
      showSelection();
      dragObject = event.point - selectionRect.topLeft;
      return;
    }    
  }
}

var mousePosition;
function onMouseMove(event) {
  event.modifiers.shift = event.event.shiftKey;
  event.modifiers.control = event.event.ctrlKey;
  event.preventDefault();
  mousePosition=new Point(event.point);
  switch (clickMode) {
  case 'continuecurve':
    dragObject.point = snapPoint(event.point);
    smoothPath(dragObject.path);
    break;
  }
  
}

function onMouseUp(event) {
  event.modifiers.shift = event.event.shiftKey;
  event.modifiers.control = event.event.ctrlKey;
  if (dragged) onMouseDragEnd(event);
  else onMouseClick(event);
}

function onMouseClick(event) {
  switch(dragMode) {
  case 'span':
    deselectAll();
    break;
  case 'selection':
//  case 'item':
    if (selectedSegments.length) {
      deselectAllSegments();
    } else toggleHandleMode();
    break;
  case 'size':
  case 'distribute':
  case 'rotate':
  case 'distrotate':
  case 'crop':
    applySelectionClone();
    break;  
  }
  showSelection();
  project.view.draw();
}


function onMouseDrag(event) {
  event.modifiers.shift = event.event.shiftKey;
  event.modifiers.control = event.event.ctrlKey;

  dragged = true;
  switch(dragMode) {
  case 'newhorizontalguide':
    if (!dragObject && event.point.y > topRulerRaster.bounds.bottom) {
      dragObject = horizontalGuideRaster.clone();
      spanLayer.addChild(dragObject);
      dragObject.position = new Point(project.view.center.x,event.point.y);
    } else if (dragObject) {
      dragObject.position.y = event.point.y;
    }
    return;
  case 'newverticalguide':
    if (!dragObject && event.point.x > leftRulerRaster.bounds.right) {
      dragObject = verticalGuideRaster.clone();
      spanLayer.addChild(dragObject);
      dragObject.position = new Point(event.point.x,project.view.center.y);
    } else if (dragObject) {
      dragObject.position.x = event.point.x;
    }
    return;
  case 'movehorizontalguide':
    dragObject.position.y = event.point.y;
    return;
  case 'moveverticalguide':
    dragObject.position.x = event.point.x;
    return;
  case 'draw':
    dragObject.add(event.point);
    return;
  case 'pan':
    var c = event.downPoint - event.point; 
    project.view.scrollBy(c*project.view.zoom) ;
    drawGrid();
    project.view.draw();
    return;
  case 'span':
    spanLayer.removeChildren();
    spanRect = new Rectangle(dragObject, event.point);
    var p = new Path.Rectangle(spanRect);
    spanLayer.addChild(p);
    p.style = {
      strokeColor:'blue',
      strokeWidth:1/project.view.zoom,
      fillColor: new RgbColor(0,0,1,0.05)
    };
    drawToolText(spanRect.bottomCenter.add(0,20/project.view.zoom),'drag to select | hold shift to add | hold control to subtract');
    return;
  case 'segment':
    var d = snapPoint(event.point) - dragObject.point;
    for (var i =0; i<selectedSegments.length; i++) {
      selectedSegments[i].point+=d;
    }
    smoothPath(dragObject.path);
    drawSelectedSegments();
    break;
  case 'item':
  case 'selection':
    if (handleMode == 'crop') {
      var delta = event.delta;
      for (var i =1, f; f = focusedItem.children[i]; i++ ) { f.translate(delta); f.apply(); }
    } else {
      var tl = snapPoint(event.point-dragObject);
      var delta = tl-selectionRect.topLeft;
      if (!tl.snapped) {
        var br = snapPoint(event.point+selectionRect.size-dragObject);
        if (br.snapped) delta = br-selectionRect.bottomRight;
      }
      for (var i in selection) { selection[i].translate(delta); selection[i].apply(); }
      for (var i in selectionLayer.children) selectionLayer.children[i].translate(delta);
      selectionRect.point += delta;
    }
    drawSelectedSegments();
    //drawSelection();
    break;
  case 'size':
  case 'distribute':
  case 'rotate':
  case 'distrotate':
  case 'gradient':
  case 'crop':
    var handle = dragObject.whichHandle || dragObject;
    var r = handleModes[handleMode].dragHandle(event,handle,dragObject);
    handleModes[handleMode].applyHandle.apply(this,[event,dragObject.whichHandle].concat(r));
    return;
  }
}


function onMouseDragEnd(event) {
  switch (dragMode) {
  case 'newhorizontalguide':
  case 'movehorizontalguide':
    dragObject.remove();
    if (event.point.y > topRulerRaster.bounds.bottom) {
      addGuide('y',event.point.y);
      drawGrid();
      undoPush();
    }
    break;
  case 'newverticalguide':
  case 'moveverticalguide':
    dragObject.remove();
    if (event.point.x > leftRulerRaster.bounds.right) {
      addGuide('x',event.point.x);
      drawGrid();
      undoPush();
    }
    break;
  case 'draw':
    if (event.modifiers.shift || snapPoint(event.point).getDistance(dragObject.segments[0].point)<5/project.view.zoom) {
      dragObject.closePath();
    } 
    dragObject.simplify(10);
    convertPath(dragObject);
    deselectAll();
    selectItem(dragObject);
    setClickMode('select');
    undoPush();
    break;
  case 'span':
    if (dragged) {
      if (!event.modifiers.shift && !event.modifiers.control) deselectAll();
      for (var i=0; i<activeItem.children.length; i++) {
        if (spanRect.contains(activeItem.children[i].bounds)) {
          if (event.modifiers.control) deselectItem(activeItem.children[i]);
          else selectItem(activeItem.children[i]);
        }
      }
      spanLayer.removeChildren();
    }
    break;
  case 'size':
  case 'distribute':
  case 'rotate':
  case 'distrotate':
  case 'crop':
    applySelectionClone();
    undoPush();
  case 'item':
  case 'selection':
  case 'push':
  case 'segment': 
    undoPush();
    break;
  }
  clearToolText ();
  dragMode = null;
  showSelection();
  project.view.draw();
}

//***************************************************************************************************************************
//                                        
//     G R I D ,   R U L E R ,   G U I D E S
//                                        
//***************************************************************************************************************************



var UI = {};

UI.guides = {x:[0],y:[0]};
UI.showGuides = true;
UI.snapToGuides = true;

UI.showGrid = true;
UI.gridX = 20;
UI.gridY = 20;
UI.snapToGrid = false;
UI.showRuler = true;

function addGuide(which,n) {
  var guides = UI.guides[which];
  if (guides.indexOf(n)>-1) return;
  guides.push(n);
  guides.sort(function(a,b) {return a-b});
}
function removeGuide(which,n) {
  var guides = UI.guides[which];
  var pos = guides.indexOf(n);
  if (pos>-1) guides.splice(pos,1);
}
function moveGuide(which,m,n) {
  removeGuide(which,m);
  addGuide(which,n);
}


function snapToGuide(which, p, tolerance) {
  tolerance = tolerance || 5;
  var guides = UI.guides[which];
  var POS = p[which];
  var DD = Math.abs(tolerance/project.view.zoom);
  var D = Infinity;
  var pos = POS;
  for (var i = 0; i<guides.length;i++) {
    var guide = guides[i];
    var d = guide - pos;
    if ( d < -DD ) continue;
    if ( d > DD ) break;
    if (Math.abs(d)<=D) {
      POS = guide;
      D = Math.abs(d);
    }
  }
  var ret = new Point(p);
  ret[which] = POS;
  ret.snapped = D !== Infinity;
  return ret;
}

function snapPoint(p) {
  var xSnapped = false;
  var ySnapped = false;
  var X = p.x;
  var Y = p.y;
  
  if (UI.snapToGuides) {
    var pp = snapToGuide('x',p);
    X = pp.x;
    xSnapped = pp.snapped;
    var pp = snapToGuide('y',p);
    Y = pp.y;
    ySnapped = pp.snapped;
  }
  
  if (UI.snapToGrid) {
    if (!xSnapped) X = roundTo(p.x,UI.gridX);
    if (!ySnapped) Y = roundTo(p.y,UI.gridY);
    xSnapped = true;
    ySnapped = true;
  }
  var ret = new Point(X,Y);
  ret.snapped = xSnapped || ySnapped;
  return ret;
}

var gridRaster = null;
var topRulerRaster = null;
var leftRulerRaster = null;
var verticalGuideRaster = null;
var horizontalGuideRaster = null;

function drawGrid(redraw) {
  gridLayer.removeChildren();
  guideLayer.removeChildren();
  rulerLayer.removeChildren();
  
  if (!redraw && !UI.showGrid && !UI.showRuler && !UI.showGuides) return;
  
  var gx = UI.gridX;
  var gy = UI.gridY;
  while (gx * project.view.zoom < 10) gx *= 2;
  while (gy * project.view.zoom < 10) gy *= 2;
  var b = project.view.getBounds();
  var x1 =  b.x - b.x % (gx*10) - gx*10.5;
  var y1 =  b.y - b.y % (gy*10) - gy*10.5;
  if (!gridRaster || redraw) {
    var g = new Group();
    for (var i=0.5; i<10; i++) {
      var p = Path.Line(new Point(0,i*gy), new Point(10*gx,i*gy));
      g.addChild(p);
      var p = Path.Line(new Point(i * gx, 0), new Point(i*gx,10*gy));
      g.addChild(p);
    }
    g.style = {
      strokeColor:'#88f',
      strokeWidth:0.5/project.view.zoom,
      dashArray: [2/project.view.zoom,2/project.view.zoom]
    }
    g.children[0].style = 
    g.children[1].style = 
    g.children[10].style = 
    g.children[11].style = {
      strokeColor:'#88f',
      strokeWidth:0.5/project.view.zoom,
      dashArray: [2/project.view.zoom,0/project.view.zoom]
    }
    var r = g.rasterize(72*project.view.zoom);
    g.remove();
    
    var x2 =  b.x + 1 * b.width + r.bounds.width;
    var y2 =  b.y + 1 * b.height + r.bounds.height;
    for (var x = x1 ; x < x2 ; x += r.bounds.width) {
      for (var y = y1 ; y < y2 ; y += r.bounds.height) {
        var c = r.clone();
        c.position = new Point(x,y);
        gridLayer.addChild(c);
      }
    }
    r.remove();
    gridRaster = gridLayer.rasterize(72*project.view.zoom);
    gridRaster.remove();
    gridLayer.removeChildren();
  }
  var rx = gx;
  var ry = gy;
  var rulerWidth = 20/project.view.zoom;
  if (!topRulerRaster || redraw) {
    var g = new Group();
    var g2 = new Group();
    for (var i=0.5; i<10; i++) {
      var p = Path.Line(new Point(i * rx, 12/project.view.zoom), new Point(i*rx,rulerWidth));
      g.addChild(p);
      var p = Path.Line(new Point(12/project.view.zoom, i * ry), new Point(rulerWidth, i*ry));
      g2.addChild(p);
    }
    g.style = g2.style = {
      strokeColor:'#999',
      strokeWidth:0.5/project.view.zoom,
    }
    g.children[0].style = 
    g2.children[5].style = 
    g.children[0].style = 
    g2.children[5].style = {
      strokeColor:'#333',
      strokeWidth:1/project.view.zoom,
    }
    var back = Path.Rectangle(0,0,10*rx,rulerWidth);
    var back2 = Path.Rectangle(0,0,rulerWidth,10*ry);
    back.style = back2.style = {
      fillColor:'#f0f8ff'
    }
    g.insertChild(0,back);
    g2.insertChild(0,back2);
    var line = Path.Line(0,rulerWidth,10*rx,rulerWidth);
    var line2 = Path.Line(rulerWidth,0,rulerWidth,10*ry);
    line.style = line2.style = {
      strokeColor:'#000',
      strokeWidth:2/project.view.zoom,
    }
    g.insertChild(1,line);
    g2.insertChild(1,line2);
    var r = g.rasterize(72*project.view.zoom);
    var r2 = g2.rasterize(72*project.view.zoom);
    g.remove();
    g2.remove();
    r.remove();
    r2.remove();

    gridLayer.removeChildren();
    
    var x2 =  b.x + 1 * b.width + r.bounds.width;
    for (var x = x1 ; x < x2 ; x += r.bounds.width) {
      var c = r.clone();
      c.position = new Point(x,0);
      gridLayer.addChild(c);
    }
    topRulerRaster = gridLayer.rasterize(72*project.view.zoom);
    gridLayer.removeChildren();
    topRulerRaster.remove();
    
    var y2 =  b.y + 1 * b.height + r2.bounds.height;
    for (var y = y1 ; y < y2 ; y += r2.bounds.height) {
      var c = r2.clone();
      c.position = new Point(0,y);
      gridLayer.addChild(c);
    }
    leftRulerRaster = gridLayer.rasterize(72*project.view.zoom);
    leftRulerRaster.remove();
    gridLayer.removeChildren();
    
    var l1 = Path.Line(0,0,0,b.height);
    var l2 = Path.Line(0,0,b.width,0);

    l1.style = l2.style = {
      strokeColor:'#f00',
      strokeWidth:1/project.view.zoom,
      strokeCap: 'butt',
      dashArray: [2/project.view.zoom,2/project.view.zoom]
    }
    var r1 = l1.rasterize(72*project.view.zoom);
    var r2 = l2.rasterize(72*project.view.zoom);

    l1.remove();
    r1.remove();
    l2.remove();
    r2.remove();
    verticalGuideRaster = r1;
    horizontalGuideRaster = r2;
  }
  
  if (UI.showGrid) {
    gridLayer.addChild(gridRaster);
    gridRaster.position -= gridRaster.bounds.topLeft - new Point(x1,y1);
    gridRaster.apply();
  }
  
  if (UI.showRuler) {
    var TOP = topRulerRaster;
    var LEFT = leftRulerRaster;
    var top = b.top + 40/project.view.zoom;
    var left = b.left +54/project.view.zoom;
    var O = new Point(left,top);
    
    rulerLayer.addChild(TOP);
    TOP.position = new Point(x1,top) + TOP.bounds.size / 2;
    TOP.apply();

    rulerLayer.addChild(LEFT);
    LEFT.position = new Point(left,y1) + LEFT.bounds.size / 2;
    LEFT.apply();

    for (var x = x1 + rx/2 ; x < b.right ; x += rx * 5) {
      var t = new PointText();
      t.style = {
        fontSize:10/project.view.zoom,
        fillColor:'black',
      }
      t.setContent(x);
      t.position = new Point(x, top + 8/project.view.zoom);
      rulerLayer.addChild(t);
    }
    for (var y = y1 +ry/2; y < b.bottom ; y += ry * 5) {
      var t = new PointText();
      t.style = {
        fontSize:10/project.view.zoom,
        fillColor:'black'
      }
      t.setContent(y);
      t.position = new Point(left+8/project.view.zoom,y);
      t.rotate(-90);
      rulerLayer.addChild(t);
    }
    var patch = Path.Rectangle(O,rulerWidth);
    patch.style = {
      fillColor:'#f0f8ff',
      strokeColor:'#000',
      strokeWidth: 1/project.view.zoom
    }
    rulerLayer.addChild(patch);
  }
  
  if (UI.showGuides) {

    for (var i=0; i < UI.guides.x.length; i++) {
      var g = g = UI.guides.x[i];
      if (g<b.left) continue;
      if (g>b.right) break;
      var c = verticalGuideRaster.clone();
      c.orientation = 'vertical';
      guideLayer.addChild(c);
      c.position = new Point(g,project.view.center.y);
    }
    for (var i=0; i < UI.guides.y.length; i++) {
      var g = g = UI.guides.y[i];
      if (g<b.top) continue;
      if (g>b.bottom) break;
      var c = horizontalGuideRaster.clone();
      c.orientation = 'horizontal';
      guideLayer.addChild(c);
      c.position = new Point(project.view.center.x,g);
    }
  }
  project.view.draw();

}

drawGrid();




//***************************************************************************************************************************
//                                        
//     H A N D L E   M O D E S
//                                        
//***************************************************************************************************************************


function toggleHandleMode() {
  var mode;
  if(handleMode=='size') handleMode ='rotate';
  else handleMode = 'size';
  showSelection();
}

var initalSelectionRect = null;
initHandleDrag = function(mode,obj) {
  initalSelectionRect = new Rectangle(selectionRect);
  dragObject = obj;
  handleMode = mode;
  dragMode = handleMode;
  handleModes[handleMode].initDrag && handleModes[handleMode].initDrag();
}
var handleModes = {
  size: {
    char: '⤡', //↻⇲⟲⤡✒▥
    editSegments: true,
    drawHandle: function (which,s,ox,oy) {
      return Path.Rectangle(s.point.x-5, s.point.y-5 ,10,10);
    },
    initDrag: function () {
      initSelectionClone();
    },
    dragHandle: function(event,which) {
      var f = 1
      var fx = 1;
      var fy = 1;
      var d = snapPoint(event.point) - selectionRect[which];
      var center;
      var w = selectionRect.width;
      var h = selectionRect.height;
      switch (which) {
      case 'topLeft':
        fx = (w-d.x)/w; fy = (h-d.y)/h;
        f = Math.abs(fx) < Math.abs(fy) ? fx : fy;
        center = selectionRect.bottomRight;
        break;
      case 'topCenter': f = fy = (h - d.y)/h;
        center = selectionRect.bottomCenter;
        break;
      case 'topRight':
        fx = (w+d.x)/w;
        fy = (h-d.y)/h;
        f = Math.abs(fx) < Math.abs(fy) ? fx : fy;
        center = selectionRect.bottomLeft;
        break;
      case 'bottomLeft':
        fx = (w-d.x)/w;
        fy = (h+d.y)/h;
        f = Math.abs(fx) < Math.abs(fy) ? fx : fy;
        center = selectionRect.topRight;
        break;
      case 'leftCenter':
        f = fx = (w - d.x)/w;
        center = selectionRect.rightCenter;
        break;
      case 'rightCenter':
        f = fx = (w + d.x)/w;
        center = selectionRect.leftCenter;
        break;
      case 'bottomCenter':
        f = fy = (h + d.y)/h;
        center = selectionRect.topCenter;
        break;
      case 'bottomRight':
        fx = (w + d.x)/w;
        fy = (h+d.y)/h;
        f = Math.abs(fx) < Math.abs(fy) ? fx : fy;
        center = selectionRect.topLeft;
        break;
      }
      if (event.modifiers.shift) fx = fy = f;
      if (event.modifiers.control) {
        center = selectionRect.center;
        fx *= 2;
        fy *= 2;
        f *= 2;
      }
      return ([center,fx,fy]);
    },
    applyHandle: function(event,which,center,fx,fy) {
      var m = new Matrix().scale(fx, fy, center);
      transformSelectionClone(function(item) {
        item.transform(m); 
        item.apply()
      });
      var r = drawSelection(m).bounds;
      var p = (r.width > 200/project.view.zoom && r.height > 100/project.view.zoom) 
      ? r.center
      : r.bottomCenter.add(0,20/project.view.zoom);
      console.log(p.toString());
      drawToolText(p,(fx*100).toFixed(1) + '% × ' + (fy*100).toFixed(1) + '%\n' 
        + roundTo(fx*selectionRect.width,0.01) + ' × ' + roundTo(fy*selectionRect.height,0.01) + '');
    },
    fill: true
  }, 
  rotate: {
    char: '↻', //↻⇲⟲⤡✒▥
    editSegments: true,
    drawHandle: function (which,s) {
      if (which.indexOf('Center')>-1)
      var path = new Path.RegularPolygon(s.point, 6, 6).scale(1,1.5).rotate((new Point(1,0)).getDirectedAngle(s.next.point-s.point)+90);
      else var path = Path.Circle(s.point,6);
      return path;
    },
    initDrag: function () {
      initSelectionClone();
    },
    dragHandle: function(event,which) {
      var fx = 0;
      var fy = 0;
      var center = altcenter = selectionRect.center;
      var a = 0;
      var angle = 0;
      var d = event.point - event.downPoint;
      
      switch (which) {
      case 'topRight':
        altcenter = selectionRect.bottomLeft;
        break;
      case 'topLeft':
        altcenter = selectionRect.bottomRight;
        break;
      case 'bottomRight':
        altcenter = selectionRect.topLeft;
        break;
      case 'bottomLeft':
        altcenter = selectionRect.topRight;
        break;
      case 'leftCenter':
        center = selectionRect.rightCenter;
        break;
      case 'rightCenter':
        center = selectionRect.leftCenter;
        break;
      case 'topCenter':
        center = selectionRect.bottomCenter;
        break;
      case 'bottomCenter':
        center = selectionRect.topCenter;
        break;
      }

      if (event.modifiers.control) {
        center = altcenter;
      }
      
      var snap = 0;
      angle = (event.downPoint-selectionRect.center).getDirectedAngle(event.point-center);
      if (event.modifiers.shift) {
        if (event.point.getDistance(center,true)<150*150) {
          snap = 15;
          angle = angle - angle % 15;
        } else if (event.point.getDistance(center,true)<300*300) {
          angle = angle - angle % 5;
          snap = 5;
        } else {
          angle = angle|0;
          snap = 1;
        }
      }
     switch (which) {
      case 'topRight':
      case 'topLeft':
      case 'bottomRight':
      case 'bottomLeft':
        a = angle;
        break;
      case 'leftCenter':
      case 'rightCenter':
        fy = Math.tan(angle / 360 * 6.28318531);
        break;
      case 'topCenter':
      case 'bottomCenter':
        fx = -Math.tan(angle / 360 * 6.28318531);
        break;
      }
      return ([center,fx,fy,a,angle,snap]);
    },
    applyHandle: function(event,which,center,fx,fy,a,angle,snap) {
      var m = new Matrix().shear(fx, fy, center).rotate(a,center);
      transformSelectionClone(function(item) {
        item.transform(m); 
        item.apply()
      });
      drawSelection(m);
      var d = center.getDistance(event.point);
      var pt1 = (event.downPoint-center).normalize(project.view.bounds.width+project.view.bounds.height);
      var pt2 = (event.downPoint-center).rotate(angle).normalize(project.view.bounds.width+project.view.bounds.height);
      var p = new Path();
      p.style = {
        strokeWidth: 2,
        strokeColor: 'white'
      }
      p.moveTo(center+pt1);
      p.lineTo(center);
      p.lineTo(center+pt2);
      selectionLayer.addChild(p);
      var p2=p.clone();
      p2.style = {
        strokeWidth: 1,
        strokeColor: 'blue'
      }
      selectionLayer.addChild(p2);
      drawToolText(center,angle.toFixed(1)+'°\n' + (snap ? 'snap to '+snap+'°' : 'press shift to snap'));
    },
    fill:true
  },
  distribute: {
    char: '⤡', //↻⇲⟲⤡✒▥
    check: function() { return selectionCount>1 },
    drawHandle: function() { return handleModes.size.drawHandle.apply(this,arguments) },
    initDrag: function () {
      initSelectionClone();
    },
    dragHandle: function() { return handleModes.size.dragHandle.apply(this,arguments) },
    applyHandle: function(event,which,center,fx,fy) {
      var m = new Matrix().scale(fx, fy, center);
      transformSelectionClone(function(item) {
        item.position = item.position.transform(m); 
      });
      drawSelection(m);
    },
  }, 
  distrotate: {
    char: '↻', //↻⇲⟲⤡✒▥
    check: function() { return selectionCount>1 },
    drawHandle: function() { return handleModes.rotate.drawHandle.apply(this,arguments) },
    initDrag: function () {
      initSelectionClone();
    },
    dragHandle: function() { return handleModes.rotate.dragHandle.apply(this,arguments) },
    applyHandle: function(event,which,handle,center,fx,fy,angle) {
      var m = new Matrix().shear(fx, fy, center).rotate(angle,center);
      transformSelectionClone(function(item) {
        item.position = item.position.transform(m); 
      });
      drawSelection(m);
    },
  },
  crop: {
    char: '⇲', //↻⇲⟲⤡✒▥
    editSegments: true,
    check: function() { return selectionCount == 1  && ( focusedItem.data.kind == 'frame'  || focusedItem.data.kind == 'image') },
    drawHandle: function() { return handleModes.size.drawHandle.apply(this,arguments) },
    initDrag: function () {
      initSelectionClone();      
    },
    dragHandle: function() { return handleModes.size.dragHandle.apply(this,arguments) },
    applyHandle: function(event,which,center,fx,fy) {
      var m = new Matrix().scale(fx, fy, center);
      transformSelectionClone(function(item) {
        item.children[0].transform(m); 
        item.children[0].apply();
        item._bounds = null;      
        item._changed(Change.GEOMETRY);      
        project.view.draw();
      });
      drawSelection(m);
    },
  }, 
  gradient: {
    char: '▥', //↻⇲⟲⤡✒▥
    check: function() {  return focusedItem && focusedItem.fillColor && focusedItem.fillColor.origin},
    drawHandles: function() {
      if (focusedItem && focusedItem.fillColor && focusedItem.fillColor.origin) {
//        focusedItem.selected = false;
        var O = focusedItem.fillColor.origin;
        var D = focusedItem.fillColor.destination;
        var A = (D-O).getDirectedAngle(0,-1);
        var p1 = new Path.Circle(O, 6 / project.view.zoom);
        var p2 = new Path();
        p2.moveTo(D);
        p2.lineBy(-7.5/project.view.zoom,15/project.view.zoom);
        p2.lineBy(15/project.view.zoom,0);
        p2.closePath();
        p1.rotate(-A);
        p2.rotate(-A,D);
        var p3 = new Path.Line(O,D);
        p1.style = p2.style = p3.style = {
          strokeColor:'blue',
          strokeWidth:1/project.view.zoom,
          fillColor: 'white'
        };
        selectionLayer.addChild(p3); 
        selectionLayer.addChild(p2); 
        selectionLayer.addChild(p1); 
        p1.whichHandle = 'origin';
        p2.whichHandle = 'destination';
        clearToolText ();
      } else {
        //drawToolText(focusedItem.position,'no gradient');
      }
    },
    initDrag: function () {
    },
    dragHandle: function(event,which) {
      console.log('ff');
      focusedItem.fillColor[which]=event.point;
      getProp(focusedItem,'gradientOrigin');
      getProp(focusedItem,'gradientDest');
      return;
    },
    applyHandle: function(event,which,handle) { 
      clearToolText ();
      drawSelection();
    },
  }
}



//***************************************************************************************************************************
//                                        
//     A C T I O N S 
//                                        
//***************************************************************************************************************************

function onKeyDown(event) {
  event.modifiers.shift = event.event.shiftKey;
  event.modifiers.control = event.event.ctrlKey;
  if($(event.event.target).is(':input')) return;
  else event.event.preventDefault();
  switch (event.key) {
  case 'z':
    doAction('undo');
    break;
  case 'y':
    doAction('redo');
    break;
  case 'n':
    if (event.modifiers.shift) doAction('newSketch');
    break;
  case 'a':
    doAction('selectAll');
    break;
  case 'i':
    doAction('selectInvert');
    break;
  case 'd':
    if (event.modifiers.shift) doAction('clone');
    else doAction('duplicate');
    break;
  case 'k':
    doAction('clone');
    break;
  case 'x':
    doAction('cut');
    break;
  case 'c':
    doAction('copy');
    break;
  case 'v':
    if (event.modifiers.shift) doAction('pasteInto',[mousePosition]);
    else doAction('paste',[mousePosition]);
    break;
  case 'p':
  case 'page-up':
    if (event.modifiers.shift) doAction('moveToTop');
    else doAction('moveForward');
    break;
  case 'page-down':
    if (event.modifiers.shift) doAction('moveToBottom');
    else doAction('moveBack');
    break;
  case 'g': 
    if (event.modifiers.shift) doAction('ungroup');
    else doAction('group');
    break;
  case 'delete': 
    doAction('delete');
    break;
  case 'backspace': 
    doAction('deleteLastSegment');
    break;
  default:
    console.log(event.key);
  }
}

var currentSketchName = null;
var currentSketchVersion = null;
function setSketchName (name) {
  currentSketchName = name;
  $('#sketchname').text(name);
}
var doAction = project.doAction = function (a,args) {
  console.log(a,args);
  var a = actions[a];
  if(!a) return;
  args = args || a.getArgs && a.getArgs() || [];
  if(a.check && !a.check.apply(this,args)) return;
  console.log(a);
  a.method.apply(this,args);
  showSelection();
  project.view.draw();
}
var actions = {
   imageResetClip: {
    check: function() { return selectionCount == 1 && focusedItem.data.kind == 'image' },
    method: function() {
      resetImageClip(focusedItem);
      undoPush();
    }
  },
  delete: {
    check: function() { 
      return selectionCount>0;
    },
    method: function() {
      if (selectedSegments.length > 0) {
        while (selectedSegments.length > 0) removeSegment(selectedSegments[0]);
        smoothPath(focusedItem);
      } else {
        for (var i in selection) {
          var it = selection[i];
          deselectItem(it);
          setProps(it, {
            stroke: null,
            fill: null
          });
          it.remove();
        }
        deselectAll();
      }
      undoPush();
    }
    
  },
  moveForward : {
    check: function() { 
      return selectionCount>0;
    },
    method: function() {
      var keys = Object.keys(selection).sort(function(a,b){return b-a});
      moveSelectionAbove(selection[keys[0]].nextSibling);
      undoPush();
    }
  },
  moveToTop : {
    check: function() { 
      return selectionCount>0;
    },
    method: function() {
      moveSelectionAbove(activeItem.lastChild);
      undoPush();
    }
  },
  moveBack : {
    check: function() { 
      return selectionCount>0;
    },
    method: function() {
      var keys = Object.keys(selection).sort(function(a,b){return a-b});
      moveSelectionBelow(selection[keys[0]].previousSibling);
      undoPush();
    }
  },
  moveToBottom : {
    check: function() { 
      return selectionCount>0;
    },
    method: function() {
      moveSelectionBelow(activeItem.firstChild);
      undoPush();
    }
  },
  alignTop: {
    check: function () {
      return selectionCount > 1 ;
    },
    method: function () {
      var a = focusedItem.bounds.top;
      for (var i in selection) {
        var s = selection[i];
        s.position = new Point(s.position.x, a + s.bounds.size.height/2);
        s.apply();
      }
    }
  },
  alignMiddle: {
    check: function () {
      return selectionCount > 1 ;
    },
    method: function () {
      var a = focusedItem.position.y;
      for (var i in selection) {
        var s = selection[i];
        s.position = new Point(s.position.x, a);
        s.apply();
      }
    }
  },
  alignBottom: {
    check: function () {
      return selectionCount > 1 ;
    },
    method: function () {
      var a = focusedItem.bounds.bottom;
      for (var i in selection) {
        var s = selection[i];
        s.position = new Point(s.position.x, a - s.bounds.size.height/2);
        s.apply();
      }
    }
  },
  alignLeft: {
    check: function () {
      return selectionCount > 1 ;
    },
    method: function () {
      var a = focusedItem.bounds.left;
      for (var i in selection) {
        var s = selection[i];
        s.position = new Point(a + s.bounds.size.width/2, s.position.y);
        s.apply();
      }
    }
  },
  alignCenter: {
    check: function () {
      return selectionCount > 1 ;
    },
    method: function () {
      var a = focusedItem.position.x;
      for (var i in selection) {
        var s = selection[i];
        s.position = new Point(a,s.position.y);
        s.apply();
      }
    }
  },
  alignRight: {
    check: function () {
      return selectionCount > 1 ;
    },
    method: function () {
      var a = focusedItem.bounds.bottom;
      for (var i in selection) {
        var s = selection[i];
        s.position = new Point(a - s.bounds.size.width/2, s.position.y);
        s.apply();
      }
    }
  },
  undo: {
    check: function () {
      return Number(sessionStorage.undo_current) > Number(sessionStorage.undo_bottom) ;
    },
    method: function () {
      undoPop();
      drawSelection();
    }
  },
  redo: {
    check: function () {
      return Number(sessionStorage.undo_current) < Number(sessionStorage.undo_top) ;
    },
    method: function () {
      undoRedo();
      drawSelection();
    }
  },
  cut: {
    check: function() { return selectionCount>0; },
    method: function() {
      localStorage.clipboard = JSON.stringify(saveItems(selection));
      doAction('delete');
    }
  },
  copy: {
    check: function() { return selectionCount>0; },
    method: function() {
      localStorage.clipboard = JSON.stringify(saveItems(selection));
    }
  },
  paste: {
    check: function() { return !!localStorage.clipboard; },
    getArgs: function() { return [project.view.center]; },
    method: function(point) {
      deselectAll();
      var s = loadItems(JSON.parse(localStorage.clipboard),activeItem,point);//new Point(50/project.view.zoom)
      while (s.length) selectItem(s.shift());
      undoPush();
    }
  },
  duplicate: {
    check: function() { return selectionCount>0; },
    method: function() {
      var copy = saveItems(selection);
      var s = loadItems(copy,activeItem,selectionRect.center.add(50,50));
      deselectAll();
      while (s.length) selectItem(s.pop());
      undoPush();
    }
  },
  clone: {
    check: function() { return selectionCount>0; },
    method: function() {
      for (var i in selection) {
        var c = cloneItem(selection[i]);
        deselectItem(selection[i]);
        selectItem(c);
      }
      undoPush();
    }
  },
  pasteInto: {
    check: function() { 
      return selectionCount==1 && !!localStorage.clipboard && focusedItem.data.kind=='path';
    },
    getArgs: function() { return [project.view.center]; },
    method: function(point) {
      for (var i in selection) var s = selection[i];
      deselectAll();
      var d = []; for (var i in s.data) d[i]=s.data[i];
      
      var g = new Group();
      activeItem.addChild(g);
      g.addChild(s);
      g.clipped = true;
      blessItem(g,'frame',d);
      var l = loadItems(JSON.parse(localStorage.clipboard),g,point);
      selectItem(g);
      undoPush();
    }
  },
  openItem: {
    check: function() { 
      if (selectionCount!=1) return;
      for (var i in selection) var s = selection[i];
      return (s.data.kind=='frame' || s.data.kind=='group');
    },
    method: function() {
      for (var i in selection) var s = selection[i];
      deselectItem(s);
      activeItem = s;
    }
  },
  closeItem: {
    check: function() { 
      return activeItem != mainLayer;
    },
    method: function() {
      deselectAll();
      var a = activeItem;
      activeItem = activeItem.parent;
      selectItem(a);
    }
  },
  selectAll: {
    check: function() { 
      return true;
    },
    method: function() {
      for (var i = 0, c = activeItem.children ; i < c.length ; i++ ) selectItem(c[i]);
    }
  },
  selectInvert: {
    check: function() { 
      return true;
    },
    method: function() {
      for (var i = 0, c = activeItem.children ; i < c.length ; i++ ) toggleItem(c[i]);
    }
  },
  group: {
    check: function() { 
      return selectionCount>1;
    },
    method: function() {
      var keys = Object.keys(selection).sort(function(a,b){return selection[a].index - selection[b].index });
      if (selectionCount>1) {
        var g = new Group();
        g.data={kind:'group'};
        activeItem.addChild(g);
        for(var i=0; i<keys.length;i++) {
          selection[keys[i]].remove();
          g.addChild(selection[keys[i]]);
        }
        deselectAll();
        selectItem(g);
      }
      undoPush();
    }
  },
  ungroup: {
    check: function() { 
      return selectionCount>0;
    },
    method: function() {
      var keys = Object.keys(selection).sort(function(a,b){return selection[a].index - selection[b].index });
      for(var i=0; i<keys.length;i++) {
        var s = selection[keys[i]];
        if (s instanceof Group) {
          while(s.children.length>0) {
            var c = s.children[0];
            c.remove();
            c.clipMask=false;
            activeItem.addChild(c);
            selectItem(c);
          }
          deselectItem(s);
          s.remove();
        };
      }
      undoPush();
    }
  },  
  rotateLeft: {
    check: function() { 
      return selectionCount>0;
    },
    method: function() {
      var m = new Matrix();
      m.rotate(-45,selectionRect.center);
      for(var i in selection) selection[i].transform(m);
      undoPush();
    }
  },
  rotateRight: {
    check: function() { 
      return selectionCount>0;
    },
    method: function() {
      var m = new Matrix();
      m.rotate(45,selectionRect.center);
      for(var i in selection) selection[i].transform(m);
      undoPush();
    }
  },
  flipVertical: {
    check: function() { 
      return selectionCount>0;
    },
    method: function() {
      var m = new Matrix();
      m.scale(1,-1,selectionRect.center);
      for(var i in selection) selection[i].transform(m);
      undoPush();
    }
  },
  flipHorizontal: {
    check: function() { 
      return selectionCount>0;
    },
    method: function() {
      var m = new Matrix();
      m.scale(-1,1,selectionRect.center);
      for(var i in selection) selection[i].transform(m);
      undoPush();
    }
  },
  segmentSmooth: {
    check: function() { 
      return selectedSegments.length>0;
    },
    method: function() {
      for (var i =0, s; s = selectedSegments[i]; i++) {
        s.path.data.sharp[s.index] = false;
      }
      smoothPath(focusedItem);
      undoPush();
    }
  },
  segmentCusp: {
    check: function() { 
      return selectedSegments.length>0;
    },
    method: function() {
      for (var i =0, s; s = selectedSegments[i]; i++) {
        s.path.data.sharp[s.index] = true;
      }
      smoothPath(focusedItem);
      undoPush();
    }
  },
  pathClose: {
    check: function() { 
      return selectionCount==1 && focusedItem.data.kind == 'path' && !focusedItem.closed;
    },
    method: function() {
      focusedItem.closePath();
      smoothPath(focusedItem);
      undoPush();
    }
  },
  offset: {
    check: function() { 
      return selectionCount>0;
    },
    getArgs: function () {
      return [Number($('#offsetSize').val())];
    },
    method: function(d) {
      for(var i in selection) {
        if (selection[i].data.kind!='path') {
          deselectItem(selection[i]);
          continue;
        }
        var p = offsetPath(selection[i],d);
        convertPath(p,selection[i].data);
        deselectItem(selection[i]);
        selectItem(p);
        undoPush();
      }
    }
  },
  zoomIn: {
    method: function() {
      if (project.view.zoom>4) return;
      project.view.setZoom(Math.min(8,project.view.zoom * 2));
      drawGrid(true);
      showSelection();
    }
  },
  zoomOut: {
    method: function() {
      project.view.setZoom(Math.max(0.125,project.view.zoom / 2));
      drawGrid(true);
      showSelection();
    }
  },
  zoomToBounds: {
    method: function() {
      var b = new Rectangle(mainLayer.getBounds());
      var fy = (view.viewSize.height-50)/b.size.height;
      var fx = (view.viewSize.width-400)/b.size.width;
      var f = Math.min(fx,fy);
      f = Math.pow(2,Math.floor(Math.log(f)/Math.log(2)));
      project.view.setZoom(f);
      var b = new Rectangle(mainLayer.getBounds());
      project.view.setCenter(b.center + new Point(150,20));
      drawGrid(true);
    }
  },
  toggleSnapToGrid: {
    getArgs: function () {
      return [!$('#snaptogrid').get(0).checked];
    },
    method: function(v) {
      UI.snapToGrid = v;
    }
  },
  toggleShowGrid: {
    getArgs: function () {
      return [!$('#showgrid').get(0).checked];
    },
    method: function(v) {
      UI.showGrid = v;
      drawGrid();
    }
  },
  toggleSnapToGuides: {
    getArgs: function () {
      return [!$('#snaptoguides').get(0).checked];
    },
    method: function(v) {
      UI.snapToGuides = v;
    }
  },
  toggleShowGuides: {
    getArgs: function () {
      return [!$('#showguides').get(0).checked];
    },
    method: function(v) {
      UI.showGuides = v;
      drawGrid();
    }
  },
  toggleShowRuler: {
    getArgs: function () {
      return [!$('#showruler').get(0).checked];
    },
    method: function(v) {
      UI.showRuler = v;
      drawGrid();
    }
  },
  deleteLastSegment: {
    check: function() { 
      return clickMode == 'continuecurve';
    },
    method: function() {
      if (dragObject.previous && dragObject.index > 1) {
        removeSegment(dragObject.previous);
        smoothPath(dragObject.path);
        dragObject.path._changed(Change.GEOMETRY);
        drawSelection();
        project.view.draw();
      } else {
        dragObject.path.remove();
        setClickMode('select');
      }
    }
  },
}

function moveSelectionAbove (target) {
  var keys = Object.keys(selection).sort(function(a,b){return b-a});
  if (selection[keys[0]]!=activeItem.lastChild) {
    selection[keys[0]].remove();
    activeItem.insertChild(target.index+1,selection[keys[0]]);
  }
  target = selection[keys[0]];
  for(var i=1; i<keys.length;i++) {
    selection[keys[i]].remove();
    activeItem.insertChild(target.index,selection[keys[i]]);
    target = selection[keys[i]];
  }
  fixSelection();
  return;
}

function moveSelectionBelow (target) {
  var keys = Object.keys(selection).sort(function(a,b){return a-b});
  if (selection[keys[0]]!=activeItem.firstChild) {
    selection[keys[0]].remove();
    activeItem.insertChild(target.index,selection[keys[0]]);
  }
  target = selection[keys[0]];
  for(var i=1; i<keys.length;i++) {
    selection[keys[i]].remove();
    activeItem.insertChild(target.index+1,selection[keys[i]]);
    target = selection[keys[i]];
  }
  fixSelection();
}


//***************************************************************************************************************************
//                                        
//     I T E M S    A N D   T H E I R   P R O P E R T I E S
//                                        
//***************************************************************************************************************************

function blessItem(item,kind,props) {
  var proto = itemProps[kind];
  item.data = {
    kind: kind
  };
  proto.bless && proto.bless(item);
  setProps(item,proto.defaults);
  if (props) setProps(item,props);
}
function setProp(item,prop,val,kind) {
  if(!item) return undefined;
  var proto = itemProps[kind || item.data.kind];
  var method = proto._set || proto.set[prop] || itemProps._.set[prop];
  if(method) return method(item,val,prop);
}
function getProp(item,prop,kind) {
  if (!item) return undefined;
  var proto = itemProps[kind || item.data.kind];
  var method = proto._get || proto.get[prop] || itemProps._.get[prop];
  if (method) return method(item,prop);
  else return item.data[prop];
}

function setProps(item,props) {
  for (var i in props) setProp(item,i,props[i]);
}

function getBounds(item, kind) {
  var proto = itemProps[kind || item.data.kind];
  if (proto.getBounds) return proto.getBounds(item);
  else return item.bounds;
}

function makeItem(kind,position) {
  var item = itemProps[kind].make.apply(this,Array.prototype.slice.call(arguments,2));
  if (position) item.position = position;
  item.apply();
  project.view.draw();
  return;
}

function describeItem(item) {
  if (!item.data) return 'error';
  var m = itemProps[item.data.kind].describe;
  if(m) return m(item);
  else return item.data.kind + ' #' + item.id;
}



var itemProps = {
  _: {
    save: function(item) {
      var saved = {};
      for (var i in item.data) saved[i] = item.data[i];
      itemProps[item.data.kind]._save(item,saved);
      return saved;
    },
    defaults: {
      blendMode: 'normal',
    },
    set: {
      blendMode: function (item,m) {
        item.blendMode = m;
        item.data.blendMode = m;
      }
    },
    get : {
    },
    focus: function(item) {
      setFocus(item);
    },
    blur: function(item) {
      deselectAllSegments();
      setFocus(null);
    }
  },
  path: {
    describe: function(item) {
      var c = item.clipMask ? 'clip ' : '';
      return c + 'path, ' + item.segments.length + ' vertices, ' + (item.closed ? 'closed':'open');
    },
    load: function(saved) {
      var p = new Path();
      for (var i=0; i<saved.segments.length;i++) {
        p.add(saved.segments[i]);
      }
      findOrAddColor(saved.fill);
      findOrAddColor(saved.fill2);
      findOrAddColor(saved.stroke);
      if (saved.closed) p.closePath();
      
      convertPath(p,saved);
      return p;
    },
    _save: function(item,saved) {
      saved.closed = item.closed;
      saved.segments = [];
      for (var i in item.segments) saved.segments.push([item.segments[i].point.x,item.segments[i].point.y]);
    },
    defaults: {
      stroke: 'black',
      gradient: 'none',
      gradientOrigin: [0,0],
      gradientDest: [1,1],
      fill: 'white',
      fillopacity: 1,
      fill2: 'white',
      fill2opacity: 0,
      thickness: 2,
      jitter: 0,
      smoothness: 1
    },
    get: {
      gradientOrigin: function (item) {
        if (item.style.fillColor && item.style.fillColor.origin) {
          var p = (item.style.fillColor.origin - item.bounds.topLeft)/item.bounds.size;
          item.data.gradientOrigin = [p.x,p.y];
        }
        return item.data.gradientOrigin;
      },
      gradientDest: function (item) {
        if (item.style.fillColor && item.style.fillColor.destination) {
          var p = (item.style.fillColor.destination - item.bounds.topLeft)/item.bounds.size;
          item.data.gradientDest = [p.x,p.y];
        }
        return item.data.gradientDest;
      },
    },
    set: {
      gradient: function (item,g) {
        item.data.gradient = g;
        item.style.fillColor = gradients[item.data.gradient](item);
      },
      gradientOrigin: function (item,p) {
        item.data.gradientOrigin = p;
        item.style.fillColor = gradients[item.data.gradient](item);
      },
      gradientDest: function (item,p) {
        item.data.gradientDest = p;
        item.style.fillColor = gradients[item.data.gradient](item);
      },
      fill: function (item,c) {
        if (item.data.fill == c) return;
        unsetColor(item,'fill');
        setColor(item,'fill',c)
        item.style.fillColor = gradients[item.data.gradient](item);
      },
      fillopacity: function (item,o) {
        item.data.fillopacity = o;
        item.style.fillColor = gradients[item.data.gradient](item);
      },
      fill2: function (item,c) {
        if (item.data.fill2 == c) return;
        unsetColor(item,'fill2');
        setColor(item,'fill2',c)
        item.style.fillColor = gradients[item.data.gradient](item);
      },
      fill2opacity: function (item,o) {
        item.data.fill2opacity = o;
        item.style.fillColor = gradients[item.data.gradient](item);
      },
      stroke: function (item,c) {
        if (item.data.stroke == c) return;
        unsetColor(item,'stroke');
        item.style.strokeColor = setColor(item,'stroke',c);
      },
      thickness: function (item,n) {
        item.data.thickness = n;
        item.style.strokeWidth = n;
      },
      smoothness: function (item,n) {
        if (item.data.smoothness==n) return;
        item.data.smoothness = n;
        smoothPath(item);
      },
      jitter: function(item,n) {
        if (item.data.jitter==n) return;
        item.data.jitter = n;
        smoothPath(item);
      }
    }
  },
  text: {
    describe: function(item) {
      return 'text "'+item.content+'", ' + roundTo(getProp(item,'fontsize'),0.1) + ' px';
    },
    load: function(obj) {
      var item = new PointText(obj.point);
      item.data = {kind:'text'};
      setProps(item,obj);
      item._matrix = new Matrix(obj.matrix);
      item._changed(Change.GEOMETRY);
      return item;
    },
    _save: function(item,saved) {
      saved.point = [item.point.x,item.point.y];
      saved.matrix = item._matrix.values;
    },
    defaults: {
      content: 'insert\ntext here',
      alignment: 'center',
      font: 'Arial',
      textcolor: 'BLACK',
      outline: 0.0001,
      outlinecolor: 'BLACK',
      fontsize:12,
      fontstretch:1
    },
    get: {
      fontsize: function(item) {
        return item._matrix.scaleY * 12;
      },
      fontstretch: function(item) {
        return item._matrix.scaleX / item._matrix.scaleY;
      }
    },
    set: {
      content: function (item,c) {
       item._bounds = {};
       item.data.content = c;
       item.setContent(c);
       item.zo_raster = null;
       item._bounds = {};
      },
      font: function (item,f) {
        item.data.font = f;
        item.characterStyle._font = f;
        item.zo_raster = null;
        item._bounds = {};
      },
      fontsize: function(item,c) {
        var f = c / 12 / item._matrix.scaleY;
        item.scale(f);
        item.apply();
      },
      fontstretch: function(item,c) {
        var f = c / item._matrix.scaleX *  item._matrix.scaleY ;
        item.scale(f,1);
        item.apply();
      },
      textcolor: function (item,c) {
        unsetColor(item,'textcolor');
        item.style._fillColor = setColor(item,'textcolor',c);
      },
      outlinecolor: function (item,c) {
        unsetColor(item,'outlinecolor');
        item.style._strokeColor = setColor(item,'outlinecolor',c);
      },
      outline: function (item,n) {
        item.data.outline = n;
        item.style._strokeWidth = n;
        this.zo_raster = null;
      },
      alignment: function (item,a) {
        item.zo_raster = null;
        item.data.alignment = a;
        var p = new Point(item.bounds.point);
        item.paragraphStyle._justification = a;
        item._bounds = {};
        item.position = item.position - item.bounds.point + p;
      }
    }
  },
  group: {
    load: function(obj) {
      var item = new Group();
      item.data = {kind:'group'};
      for (var i in obj.items) {
        loadItem(obj.items[i],item);
      }
      setProps(item,obj);
      return item;
    },
    _save: function(item,saved) {
      saved.items = [];
      for (var i=0; i<item.children.length;i++) {
        saved.items.push(saveItem(item.children[i]));
      }
    },
    defaults: {
    },
    get: {
    },
    set: {
    },
  },
  frame: {
    load: function(obj) {
      var item = new Group();
      item.data = {kind:'frame'};
      loadItems(obj.items,item);
      setProps(item,obj);
      item.clipped = true;
      return item;
    },
    _save: function(item,saved) {
      saved.items = [];
      for (var i=0; i<item.children.length;i++) {
        saved.items.push(saveItem(item.children[i]));
      }
    },
    focus: function(item) {
      setFocus(item,item.children[0]);
    },
    getBounds: function (item) {
      return item.children[0] && item.children[0].getBounds();
    },
    defaults: {
      stroke: 'black',
      gradient: 'none',
      gradientOrigin: [0,0],
      gradientDest: [1,1],
      fill: 'white',
      fillopacity: 1,
      fill2: 'white',
      fill2opacity: 0,
      thickness: 2,
      jitter: 0,
      smoothness: 1
    },
    _get: function (item,prop) {
      return getProp(item.children[0],prop,'path');
    },
    _set: function (item,val,prop) {
      setProp(item.children[0],prop,val,'path');
    },
    set:{},
    get:{},
  },
  image: {
    make: function (name) {
      var item = new Group();
      var p = Path.Rectangle(0,0,1,1);
      convertPath(p);
      item.addChild(p);
      activeItem.addChild(item);
      blessItem(item,'image');
      placeImage(item,name,function(r) {
        resetImageClip(item);
      });
      return item;
    },
    load: function(obj) {
      var item = new Group();
      item.pathItem = loadItem(obj.path,item);
      item.data = {kind:'image'};
      placeImage(item,obj.image, function (r) {
        r.transform(new Matrix(obj.matrix));
        r.position = item.position + new Point(obj.delta);
        r.apply();
      });   
      item.clipped = true;
      setProps(item,obj);
      return item;
    },
    _save: function(item,saved) {
      saved.path = saveItem(item.pathItem);
      saved.image = item.imageName;
      saved.matrix = item.rasterObject ? item.rasterObject._matrix.values : [] ;
      saved.delta = item.rasterObject.position - item.position;
    },
    focus: function(item) {
      setFocus(item,item.children[0]);
    },
    getBounds: function (item) {
      return item.children[0] && item.children[0].getBounds();
    },
    defaults: {
      stroke: 'black',
      gradient: 'none',
      gradientOrigin: [0,0],
      gradientDest: [1,1],
      fill: 'white',
      fillopacity: 1,
      fill2: 'white',
      fill2opacity: 0,
      thickness: 2,
      jitter: 0,
      smoothness: 1
    },
    _get: function (item,prop) {
      return getProp(item.children[0],prop,'path');
    },
    _set: function (item,val,prop) {
      setProp(item.children[0],prop,val,'path');
    },
    set:{},
    get:{},
  }
}


var imageUsage = {};
var imageUsageCount = {};
function placeImage(item,name,a1,a2) {
  var fn = a2 || a1;
  var version = a2 && a1 || 'current';
  var v = version+'_'+name;

  imageUsage[v] = imageUsage[v] || {};
  imageUsage[v][item.id] = item;
  imageUsageCount[v] = (imageUsageCount[name]|0) + 1;
  
  getImage(name,version, function (img) {
    var m = new Matrix();
    m.translate(item.position);
    var r = new Raster(img, m);

    item.apply();
    item.addChild(r);
    item.rasterObject = r;
    item.imageName = name;
    fn(r);
  });
}
function removeImage(item) {

  item.rasterObject.remove();
  delete item.imageName;

  var v = version+'|'+name;
  delete imageUsage[name][item.id];
  imageUsageCount[v] = (imageUsageCount[name]|0) - 1;
  if (imageUsageCount[name]<0) throw('image usage trouble');
}

function resetImageClip(item) {
  item.pathItem && item.pathItem.remove();
  project.view.draw();
  var p = item.pathItem = Path.Rectangle(item.rasterObject.bounds);
  item.insertChild(0,p);
  convertPath(p);
  item.clipped = true;
  setProp(p,'smoothness',0);
  setProp(p,'jitter',0);
  smoothPath(p);
}

//***************************************************************************************************************************
//                                        
//     C O L O R S   A N D   G R A D I E N T S 
//                                        
//***************************************************************************************************************************


project.usedColorCount = 0;
project.usedColors = {};

function colorToHex(c) {
  var rgb = (c.blue*255) | ((c.green*255) << 8) | ((c.red*255) << 16);
  return '#' + ('000000' + rgb.toString(16)).slice(-6).toUpperCase();
}

function addColor(c,name) {
  var named = !!name;
  name = name || c;
  c = c.toUpperCase();
  name = name.toUpperCase();
  while (name in project.usedColors) name += '_';
  var color = new Color(c);
  c = colorToHex(color);
  project.usedColors[name] = {
    color: color,
    hex: c,
    usage: {
      fill: 0,
      fill2: 0,
      stroke: 0
    },
    totalUsage: 0,
    named: named,
    $swatch: 
      $('<span></span>')
      .attr({
        'class': "swatch",
        'swatch-name': name,
        'swatch-color': c,
      })
      .css('background',c)
      .appendTo('#palette')
  }
  project.usedColorCount++;
  return name; 
}
function findColor(c) {
  c = c.toUpperCase();
  for (var i in project.usedColors) {
    if (i == c || project.usedColors[i].hex == c) return i;
  }
}
function  findOrAddColor(c) {
  return findColor(c) || addColor(c);
}

function maybeRemoveColor(c) {
  if (!project.usedColors[c].named && project.usedColors[c].totalUsage<=0) {
    project.usedColors[c].$swatch.remove();
    delete project.usedColors[c];
  }
}

function unsetColor(item,which) {
  var c = item.data[which];
  var col = project.usedColors[c];
  if(col) {
    getProp(item,'gradientOrigin');
    getProp(item,'gradientDest');
    col.totalUsage--;
    col.usage[which]--;
    item.data[which] = null;
    maybeRemoveColor(c);
    return col.color;
  }
  return null;
}
function setColor(item,which,c) {
  if (c) {
    c = c.toUpperCase();
    var col = project.usedColors[findOrAddColor(c)];
    col.usage[which]++;
    col.totalUsage++;
    item.data[which] = c;
    return col.color;
  } else {
    item.data[which] = null;
    return null;
  }
}

var gradients = {
  none: function(item) {
    if (!item.closed) return null;
    if (!project.usedColors[item.data.fill]) return null;
    var c = project.usedColors[item.data.fill].color.clone();
    c.setAlpha(item.data.fillopacity); 
    return c;
  },
  linear: function(item) {
    if (!item.closed) return null;
    if (!project.usedColors[item.data.fill] || !project.usedColors[item.data.fill2]) return null;
    var c1 = project.usedColors[item.data.fill].color.clone();
    c1.setAlpha(item.data.fillopacity); 
    var c2 = project.usedColors[item.data.fill2].color.clone();
    c2.setAlpha(item.data.fill2opacity); 
    var s1=new GradientStop(c1,0);
    var s2=new GradientStop(c2,1);
    var g=new Gradient([s1,s2]);
    var orig = new Point(item.data.gradientOrigin);
    var dest = new Point(item.data.gradientDest);
    return new GradientColor(g, item.bounds.point + item.bounds.size * orig,item.bounds.point + item.bounds.size * dest);
  },
  radial: function(item) {
    if (!item.closed) return null;
    if (!project.usedColors[item.data.fill] || !project.usedColors[item.data.fill2]) return null;
    var c1 = project.usedColors[item.data.fill].color.clone();
    c1.setAlpha(item.data.fillopacity); 
    var c2 = project.usedColors[item.data.fill2].color.clone();
    c2.setAlpha(item.data.fill2opacity); 
    var s1=new GradientStop(c1,0);
    var s2=new GradientStop(c2,1);
    var g=new Gradient([s1,s2],'radial');
    var orig = new Point(item.data.gradientOrigin);
    var dest = new Point(item.data.gradientDest);
    return new GradientColor(g, item.bounds.point + item.bounds.size * orig,item.bounds.point + item.bounds.size * dest);
  },
}

//***************************************************************************************************************************
//                                        
//     P A T H S                
//                                        
//***************************************************************************************************************************

function convertPath(path, opt) {
  opt = opt || {};
  path.data = {
    kind: 'path',
    jitterIn: opt.jitterIn || [],
    jitterOut: opt.jitterOut || [],
    sharp:opt.sharp || [],
    jitter: opt.jitter || 0,
    smoothness: opt.smoothness || 1
  };
  setProps(path,itemProps.path.defaults);
  for (var i = 0; i<path.segments.length; i++) {
    path.data.jitterIn.push(Math.random());
    path.data.jitterOut.push(Math.random());
  }
  smoothPath(path);
  setProps(path,opt);
  
  return path;
}
function addSegment(path,point,index) {
  if (index===undefined) index = path.segments.length;
  var s = new Segment(point);
  path.insert(index,s);
  path.data.jitterIn.splice(index,0,Math.random());
  path.data.jitterOut.splice(index,0,Math.random());
  path.data.sharp.splice(index,0,false);
  return s;
}

function removeSegment(segment) {
  deselectSegment(segment);
  var path = segment.path;
  path.data.jitterIn.splice(segment.index,1);
  path.data.jitterOut.splice(segment.index,1);
  path.data.sharp.splice(segment.index,1);
  segment.remove();
  return;
}


function smoothPath(p) {
  if (true) {
      spanLayer.removeChildren();
    for (var i = 0, s; s = p.segments[i]; i++) {
//      var v = (s.next.point - s.previous.point).normalize();
      s.handleIn = new Point(0,0);
      s.handleOut = new Point(0,0);
      if (!s.previous && s.next && s.next.next) {
        var tangent = s.next.next.point - s.point;
        
/*
        var vNext = (s.next.next.point - s.next.point);
        var vPrev = (s.point - s.next.point);
        var alpha = vPrev.getDirectedAngle(vNext)/2;
        var v = vPrev.rotate(-alpha).rotate(90);
        s.handleOut = -vPrev.project(v) * 0.2761424 * 2;
        s.handleIn = -s.handleOut;
        console.log('fff');
*/        
      } else if (!s.next && s.previous && s.previous.previous) {
        var tangent = s.point - s.previous.previous.point;
/*      
        var vNext = (s.previous.point - s.point);
        var vPrev = (s.previous.previous.point - s.previous.point);
        var alpha = vPrev.getDirectedAngle(vNext)/2;
        var v = vNext.rotate(-alpha).rotate(90);
        s.handleOut = -vNext.project(v) * 0.2761424 * 2;
        s.handleIn = -s.handleOut;
        console.log('ggg');
*/        
      } else if (s.next && s.previous){
        var tangent = s.next.point - s.previous.point;
/*
        var half = s.previous.point + tangent / 2;
        var p1 = half + (s.point-half).project(tangent);
        var p2 = half + (tangent/3).rotate(-90);

        var l = Path.Line(s.previous.point,s.next.point);
        l.style = {
          strokeColor:'orange'
        }
        spanLayer.addChild(l);

        //var l = Path.Line(s.point , p1);
        //l.style = {
        //  strokeColor:'red'
        //}
        //spanLayer.addChild(l);

        var l = Path.Line(half , p2);
        l.style = {
          strokeColor:'blue'
        }
        spanLayer.addChild(l);

        
        //var v = (p2 - s.point).normalize().rotate(90);
        //var v = tangent.normalize();
        var vNext = (s.next.point - s.point);
        var vPrev = (s.previous.point - s.point);
        var alpha = vPrev.getDirectedAngle(vNext)/2;
        var v = vPrev.rotate(alpha).rotate(-90);
        
        //var v = ((s.next.point+s.previous.point)/2-s.point).rotate(90);
        //var v = (s.next.point-s.previous.point).normalize();
//        s.handleIn = vPrev.project(v); //* 0.2761424 * 2;
//        s.handleOut = vNext.project(v); //* 0.2761424 * 2;
*/
        s.handleIn = -tangent * 0.2761424;
        s.handleOut = tangent * 0.2761424;
      } 
    }
  } else {
    p.smooth();
  }
  
  for (var i = 0; i<p.segments.length; i++) {
    var s = p.segments[i];
    var jitterIn = p.data.jitterIn[i] * p.data.jitter - p.data.jitter/2;
    var jitterOut = p.data.jitterOut[i] * p.data.jitter - p.data.jitter/2;
    if (p.data.smoothness == 0) {
      if (s.previous) s.handleIn = (s.previous.point - s.point)/2;
      if (s.next) s.handleOut = (s.next.point - s.point)/2;
      if (p.data.jitter) {
        s.handleIn = s.handleIn.rotate( jitterIn * 20);
        s.handleOut = s.handleOut.rotate(jitterOut * 20);
      }
    } else if (p.data.sharp[i]){
      s.handleIn*=0;
      s.handleOut*=0;
    } else if (p.data.smoothness) {
      s.handleIn = s.handleIn *  p.data.smoothness// * 1.1;
      s.handleOut = s.handleOut.normalize() * s.handleIn.length; 
    }
/*      if (p.data.jitter) {
        s.handleIn = s.handleIn.rotate((jitterIn * -30)/p.data.smoothness);
        s.handleOut = s.handleOut.rotate((jitterOut * -30)/p.data.smoothness);
      }
//      s.handleIn = s.handleIn.rotate( 10 + jitterIn * p.data.jitter * 20-10);
//      s.handleOut = s.handleOut.rotate( 10 + jitterOut * p.data.jitter * 20-10);
    }
*/  }
}

function offsetPath(path,d,removeOriginal) {
  if (!path.clockwise) d = -d;  
  var copy = path.clone();
  for (var i = 0; i<path.segments.length;i++) {
    var s = path.segments[i];
    if (!s.previous) {
      copy.segments[i].point = s.point + (s.next.point - s.point).normalize(d).rotate(90);
    } else if (!s.next) {
      copy.segments[i].point = s.point + (s.previous.point - s.point).normalize(d).rotate(-90);
    } else {
      var vNext = (s.next.point - s.point).normalize();
      var vPrev = (s.previous.point - s.point).normalize();
      var alpha = vPrev.getDirectedAngle(vNext)/2;
      
      var p1 = s.point + vPrev.rotate(-90) * d;
      var p2 = p1 - vPrev * d / Math.tan(alpha/180*Math.PI);
      copy.segments[i].point = p2;
    }
  }
  path.parent.insertChild(path.index+(d>0?1:0),copy);
  if(removeOriginal) path.remove();
  return copy;
}

//***************************************************************************************************************************
//                                        
//     U N D O   S T A C K
//                                        
//***************************************************************************************************************************

var updateExplorerTimer = 0;

function updateExplorer() {
  clearTimeout(updateExplorerTimer);
  updateExplorerTimer = setTimeout(doUpdateExplorer,100);
}

function doUpdateExplorer() {
  var $x = $('#explorer');
  $x.empty();
  function addItem(item,$where) {
    var $label;
    if (item==mainLayer) {
      $label = $('<span class="label mainlayer">main layer</span>');
    } else if (!item.data) {
      $label = $('<span class="label mainlayer">error</span>');
    } else {
      $label = $('<span class="label">'+describeItem(item)+'</span>');
      $label.mousedown(function(e) {
        e.preventDefault();
        if (e.shiftKey) {
          if (activeItem == item.parent) selectItem(item);
        } else if (e.ctrlKey) {
          deselectItem(item);
        } else {
          deselectAll();
          activeItem = item.parent;
          selectItem(item);
        }
        showSelection();
        updateExplorer();
        project.view.draw();
      })
    }

    if (item.children && item.children.length) {
      var $it = $('<details class="item"></details>').append($('<summary></summary>').append($label));
      if (item == activeItem || item.isAncestor(activeItem) || (selectionCount == 1 && item == focusedItem)) $it.attr('open','open');
      $it.prependTo($where);
      var $list = $('<div class="children"></div>').appendTo($it);
      for (var i = 0; i<item.children.length; i++ ) addItem(item.children[i],$list);
    } else {
      var $it = $('<div class="item"></div>').append($label);
      $it.prependTo($where);
    }
    if (item.id in selection) $it.addClass('selected');
    if (item == activeItem) $it.addClass('active');
    if (item == focusedItem) $it.addClass('focused');
  } 
  addItem(mainLayer,$x);
}


var session = sessionStorage;

function undoInit(prop,val) {
  prop = 'undo_'+prop;
  if(isNaN(Number(session[prop]))) session[prop]=val;
  session[prop] = Number(session[prop]);
}

undoInit('max',20);
undoInit('bottom',0);
undoInit('top',-1);
undoInit('current',-1);
console.log(session);

var undoReset = project.undoReset = function undoReset() {
  session.undo_max = 20;
  session.undo_bottom = 0;
  session.undo_top = -1;
  session.undo_current = -1;
}

function undoPush() {
  session.undo_current = Number(session.undo_current) + 1;
  var ui = {}; for (var i in UI) ui[i]=UI[i];
  var state = {
    ui: ui,
    items: saveItems(mainLayer.children)
  }
  session['undo_stack_' + Number(session.undo_current)] = JSON.stringify(state);
  session.undo_top = session.undo_current;
  while (Number(session.undo_current) > Number(session.undo_max) + Number(session.undo_bottom)) {
    delete session['undo_stack_'+ Number(session.undo_bottom)];
    session.undo_bottom = Number(session.undo_bottom) + 1;
  }
}
function undoPop() {
  console.log('undoing');
  undoMove(-1);
}
function undoRedo() {
  undoMove(1);
}
function undoMove(n) {
  console.log('undoing');
  var cur = Number(session.undo_current);
  var bottom = Number(session.undo_bottom);  
  var top = Number(session.undo_top);
  
  console.log(cur,bottom,top);
  if (cur + n < bottom || cur+n > top) return;
  session.undo_current = cur + n;
  deselectAll();
  mainLayer.removeChildren();
  var state = JSON.parse(session['undo_stack_'+ session.undo_current]);
  loadItems(state.items,mainLayer);
  for (var i in state.ui) UI[i]=state.ui[i];
  drawGrid();
}


//***************************************************************************************************************************
//                                        
//     S E R I A L I Z A T I O N 
//                                        
//***************************************************************************************************************************

function saveItem(item) {
  var save = (itemProps[item.data.kind].save || itemProps._.save)(item);
  if (item.id in selection) save.selected = true;
  if (item == focusedItem) save.focused = true;
  if (item == activeItem) save.active = true;
  return save;
}
function saveItems(items) {
  var ret = [];
  if (items.length) {
    for (var i=0; i<items.length;i++) ret.push(saveItem(items[i]));
  } else {
    for (var i in items) ret.push(saveItem(items[i]));
  }
  return ret;
}


function loadItem(obj,where) {
  var item = itemProps[obj.kind].load(obj);
  where = where || activeItem;
  where.addChild(item);
  return item;
}

var loadItems = function (items,where,position,noselect) {
  if (!noselect) deselectAll();
  where = where || activeItem;

  var sel = {};
  var active = null;
  var focused = null;

  var rect = null;
  var loaded = [];
  for (var i = 0; i<items.length; i++) {
    var l = loadItem(items[i],noselect);
    loaded.push(l);
    where.addChild(l);
    if (!rect) rect = new Rectangle(l.getBounds());
    else rect.unite(l.getBounds());
    if (items[i].selected) sel[l.id] = l;
    if (items[i].active) active = l;
    if (items[i].focused) focused = l;
    l.apply();
  }
  if (position) {
    var offset = position - rect.center;
    for (var i = 0; i<loaded.length; i++) {
      loaded[i].position += offset;
      loaded[i].apply();
    }
  }
  where.apply();  
  if (!noselect) {
    //deselectAll();
    activeItem = where;
    if(active) activeItem = active;
    for (var i in sel) selectItem(sel[i]);
    if (focused) focusItem(focused);
    showSelection();
  }
  return loaded;
}

function cloneItem(item) {
  var l = loadItem(saveItem(item));
  l.remove();
  if (item.parent) {
    item.parent.insertChild(item.index+1,l);
  }
  return l;
}

var saveSketch = window.saveSketch = project.saveSketch = function () {
  var saved = {
    view: {
      x: project.view.center.x,
      y: project.view.center.y,
      zoom: project.view.zoom,
    },
    ui: {},
    items:[]
  };
  for (var i in UI) saved.ui[i]=UI[i];
  saved.items = saveItems(mainLayer.children);
  return saved;
}

var newSketch = window.newSketch =  project.newSketch = function () {
  prepareSketch();
  undoReset();
  undoPush();
}

var prepareSketch = project.prepareSketch = function () {
  deselectAll();
  activeItem = mainLayer;
  mainLayer.removeChildren();
  project.view.setCenter(0,0);
  project.view.setZoom(1);
  project.usedColors = {};
  $('#palette').empty();  
  addColor('black','black');
  addColor('gray','gray');
  addColor('silver','silver');
  addColor('white','white');
  addColor('red','red');
  addColor('#ffa600','orange');
  addColor('yellow','yellow');
  addColor('green','green');
  addColor('cyan','cyan');
  addColor('blue','blue');
  addColor('magenta','magenta');
  addColor('purple','purple');

  setClickMode('select');
  drawGrid(true);
  showSelection();
}


var openSketch = window.openSketch = project.openSketch = function (obj) {
  prepareSketch();
  loadItems(obj.items);
  for (var i in obj.ui) UI[i]=obj.ui[i];
  project.view.setCenter(obj.view.x,obj.view.y);
  project.view.setZoom(obj.view.zoom);
  drawGrid(true);
  undoReset();
  undoPush();
}

//***************************************************************************************************************************
//                                        
//     J Q U E R Y   -  I N S P E C T O R  etc.
//                                        
//***************************************************************************************************************************


$(document).on('input','.data.text',function() {
  var $this = $(this);
  var prop = $this.attr('prop');
  var val = $this.val();
  
  for (var i in selection) {
    setProp(selection[i],prop,val);
  }
  project.view.draw();
  showSelection();
});

$(document).on('change','.data',function() {
  var $this = $(this);
  var prop = $this.attr('prop');
  var val = $this.val();
  
  if ($this.hasClass('number')) val = Number(val);
  if ($this.hasClass('text')) val = String(val);
  if ($this.hasClass('array')) val = String(val).split(/\s*,\s*/).map(function(v){return 0|v});
  
  for (var i in selection) {
    setProp(selection[i],prop,val);
  }
  undoPush();
  project.view.draw();
  showSelection();
  project.view.draw();
});


$(document).on('mousedown','.action',function() {
   doAction($(this).attr('action'));
   showSelection();
   project.view.draw();
});


$('button.tool').click(function() {
  setClickMode($(this).attr('tool'));
});

$(document).on('mousedown','.swatch',function (e) {
  e.preventDefault();
  if(e.which!=1) return;
  var c = $(this).attr('swatch-name');
  if (e.shiftKey) {
    for (var i in selection) setProp(selection[i],'fill2',c);
  } else {
    for (var i in selection) setProp(selection[i],'fill',c);
  }
  showSelection();
  undoPush();
  
  project.view.draw();
})
$(document).on('contextmenu','.swatch',function (e) {
  e.preventDefault();
  var c = $(this).attr('swatch-name');
  for (var i in selection) setProp(selection[i],'stroke',c);
  showSelection();
  undoPush();
  project.view.draw();
});

$(document).click( function(e) {
  var $this = $(e.target);
  $('div.menu').not($this.closest('.leaveopen, .head').closest('div.menu')).removeClass('open expanded');
});
$(document).on('mousedown','.menu .head', function(e) {
  var $menu = $(this).closest('.menu');
  if ($menu.hasClass('open')) $menu.removeClass('open');
  else {
    $('.menu').removeClass('open'); 
    $menu.addClass('open');
  }
});
$(document).on('mousedown','.menu .switchtobar', function(e) {
  $(this).closest('.menu').removeClass('menu').addClass('bar');
});
$(document).on('mousedown','.bar .switchtobar', function(e) {
  $(this).closest('.bar').removeClass('bar').addClass('menu');
});
$(function() {
  $('canvas').bind('mousewheel', function(e) {
    e.preventDefault();
    e = e.originalEvent;
    var p1 =  project.view.viewToProject(e.offsetX,e.offsetY);
    if (e.wheelDelta>0) {
      doAction('zoomIn');
    } else {
      doAction('zoomOut');
    }
    var p2 =  project.view.viewToProject(e.offsetX,e.offsetY);
    var d = (p1-p2) * project.view.zoom;
    project.view.scrollBy(d);
    drawGrid();
  });
  $('.color').on('change', function(hex,rgb) {
    var $this = $(this);
    var prop = $this.attr('prop');
    for (var i in selection) {
      var c = findOrAddColor(hex);
      setProp(selection[i],prop,c);
      maybeRemoveColor(c);
    }
    project.view.draw();
  
  });
});


var drawingMainLayer = false;
var drawingActiveItem = false;
var item

project.hookBeforeDraw = function (item,ctx,param) {
  if (param.clipping) return;  
  ctx.save();
  var activeAncestor = item.isAncestor(activeItem);
  
  if (drawingMainLayer) {
    if (item == activeItem) drawingActiveItem = true;
    if (!drawingActiveItem) {
      if (!activeAncestor && item.parent.isAncestor(activeItem)) ctx.globalAlpha = 0.15;
    }
    if (item.isClipped && (item == activeItem || activeAncestor)) {
      var c = item.clipItem;
      ctx.save();
      ctx.globalAlpha = 0.01;
      for (var i=0; i<item.children.length;i++) {
        var c = item.children[i];
        if (c == activeItem || c.isAncestor(activeItem) || c.isDescendant(activeItem)) {
           item.children[i].draw(ctx,param);
        }
      }
      ctx.restore();
      if (true || item == activeItem) {
        ctx.save();
        ctx.globalAlpha = 0.35;
        getGlobalMatrix = project.getGlobalMatrixCache();
        mx = item._matrix;
        for (var i in activeItem.children) {
          var m = getGlobalMatrix(activeItem.children[i],mx.clone());
          continue;
        }
        
        for (var i in activeItem.children) {
          var c = activeItem.children[i];
          var m = getGlobalMatrix(c,mx.clone());

          ctx.strokeStyle = 'rgba(255,255,255,1)';
          ctx.lineWidth = 4;

          if (c.drawSelected) c.drawSelected(ctx,m,true);
          else Item.drawSelectedBounds(getBounds(c),ctx,mx,true);

          ctx.strokeStyle = c._style._fillColor ? c._style._fillColor.toCssString() : 'rgba(0,0,0,1)';
          ctx.lineWidth = 2 ;

          if (c.drawSelected) c.drawSelected(ctx,m,true);
          else Item.drawSelectedBounds(getBounds(c),ctx,mx,true);
        }
        ctx.restore();
      }
    }
  }


  if (item == mainLayer) drawingMainLayer = true;
}
project.hookAfterDraw = function (item,ctx,param) {
  if (param.clipping) return;
  if (item == activeItem) drawingActiveItem = false;
  if (item == mainLayer) drawingMainLayer = false;
  ctx.restore();
}

project.hookDrawSelected = function (ctx) {
  getGlobalMatrix = project.getGlobalMatrixCache();
  mx = project.view._matrix;
  /*
  if (activeItem != mainLayer && activeItem.isClipped) {
    ctx.save();
//    Item.drawSelectedBounds(activeItem,ctx,mx,true);
    for (var i in activeItem.children) {
      var m = getGlobalMatrix(activeItem.children[i],mx.clone());
      continue;
    }

    
    for (var i in activeItem.children) {
      var c = activeItem.children[i];
      var m = getGlobalMatrix(c,mx.clone());

      ctx.strokeStyle = 'rgba(255,255,255,1)';
      ctx.lineWidth = 2;

      if (c.drawSelected) c.drawSelected(ctx,m,true);
      else Item.drawSelectedBounds(getBounds(c),ctx,mx,true);

      ctx.strokeStyle = 'rgba(0,0,255,1)';
      ctx.lineWidth = 1 ;

      if (c.drawSelected) c.drawSelected(ctx,m,true);
      else Item.drawSelectedBounds(getBounds(c),ctx,mx,true);

    }
    ctx.restore();
  }
  ctx.save();
  */
  var seg = segmentItemClone || segmentItem;
  
  if (seg && seg.drawSelected) {
    var m = getGlobalMatrix(seg,mx.clone());
    var m = getGlobalMatrix(seg,mx.clone());
//    ctx.strokeStyle = 'rgba(255,255,255,1)';
//    ctx.lineWidth = 1.5;
//    segmentItem.drawSelected(ctx,m,true);
    ctx.strokeStyle = selectedSegments.length ? 'rgba(255,255,0,1)' : 'rgba(192,192,192,1)';
    ctx.lineWidth = 0.5;
    seg.drawSelected(ctx,m,false);
  }
  ctx.restore();
}

function onResize() {
  drawGrid(true);
}
/*
cd (function() {
  cd()
  .mkdir('/zvg/sketch')
  .mkdir('/zvg/image')
  .mkdir('/zvg/preview');
})
*/
  prepareSketch();
  undoMove(0);

