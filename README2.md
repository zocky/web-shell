## Warning
This is just a prototype at a very early stage. It's bound to be full of security holes and bugs. Don't use it on the open internet, 
and don't trust it with any important files.

## Install

````
#Download from the repo
git clone https://zocky@bitbucket.org/zocky/web-shell.git
bash bin/update sh
````

You will also need `yarn` and `nodemon`.

## Run dev

The WebShell server runs as the user that's running the script, but
sudo privileges are needed to start the fs-host server.

### Using a self-signed certificate

(This might be too much trouble at this point, it's probably easier to test with local HTTP, see below).

To create a self-signed wildcard certificate in `web-shell/etc/ssl`:
````
cd web-shell
bash bin/create-certificate sh
````
Since we're using a self-signed certificate at this point, you should import it in your browser, either directly from the `.crt` file,or by exporting it in the browser. 

Alternatively, you can run WebShell on http, but it will be accessible only from the localhost.

````
cd web-shell
yarn dev
````

Go to https://desktop.127-0-0-1.sslip.io:34257 


### Using http, only accessible from the localhost

````
cd web-shell
HTTP=1 yarn dev
````
Go to http://desktop.127-0-0-1.sslip.io:34257 

## Now what

At this point, you don't need to log into the desktop (this will soon change), but the desktop doesn't have any direct access to your files, so nothing scary is happening yet.

### Files
Login to the local host filesystem connector with your linux credentials. This will establish a connection to a server process
running as your user on the host. 

It will also login your browser to the host FS server, but only at a random path, that is only known to the server and your WebShell.
This is a minimal security measure (should be tightened) to prevent other sites from opening your FS connector in an iframe 
and gaining control of it.

After that, you can browse your filesystem in the file manager.

This is just one of many possible FS connectors. The file system is entirely virtual, and actual access to files in implemented in FS connectors.

### Apps

Click the shell icon in top-left corner to open an app. In the apps, try opening, editing and saving files.

Check out the iframe source of each app to see how it's hooked up 
to work with WebShell.

Note that apps don't have any access to the file system, and that they contain no code for file handling or the UI needed for file
operations. 

The app describes the file types it can handle in its manifest, and implements file-save and file-open events.