#!/bin/bash

GREEN='\033[1;32m'
RED='\033[1;31m'
NC='\033[0m' 

SCRIPT=$(readlink -f "$0")
ROOT=$(dirname $(dirname "$SCRIPT"))

cd $ROOT;
echo -e ${GREEN}Updating WebShell${NC};
git pull;
cd $ROOT/server; yarn
cd $ROOT/client; yarn
cd $ROOT/fs/host; yarn
echo -e ${GREEN}Done.${NC};
