#!/bin/bash

SCRIPT=$(readlink -f "$0")

sudo echo "Thanks."

GREEN='\033[1;32m'
RED='\033[1;31m'
NC='\033[0m' 

ROOT=$(dirname $(dirname "$SCRIPT"))

wait_all() {
  for job in `jobs -p`
  do
  echo $job
      wait $job ;
  done
}

kill_all() {
  sudo echo Thanks.
  for job in `jobs -p`
  do
    echo Killing $job.
    sudo kill $job 2> /dev/null
  done
  echo All jobs killed.
  exit 0;
}


trap 'kill_all' SIGINT
cd $ROOT/fs/host; sudo -H yarn dev &
cd $ROOT/server;  yarn dev 

wait_all;

