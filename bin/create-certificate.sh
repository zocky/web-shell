#!/bin/bash 

GREEN='\033[1;32m'
RED='\033[1;31m'
NC='\033[0m' 

SCRIPT=$(readlink -f "$0")
ROOT=$(dirname $(dirname "$SCRIPT"))
ETC=$ROOT/etc
ETCSSL=$ETC/ssl

DEFAULT_DOMAIN=127-0-0-1.sslip.io

echo -e ${GREEN}Creating SSL certificate${NC}

DOMAIN=$1

echo "Creating a self-signed certificate."

if [ -z "$DOMAIN" ]; then
  read -p "Enter your domain [$DEFAULT_DOMAIN]: " DOMAIN
  DOMAIN=${DOMAIN:-${DEFAULT_DOMAIN}}
  echo $DOMAIN
  fi

OpenSSLConf="$ETCSSL/$DOMAIN"-openssl.cnf

cat >"$OpenSSLConf" <<EOL
[req]
req_extensions = v3_req
distinguished_name = req_distinguished_name
[ req_distinguished_name ]
countryName                 = Country
countryName_default         = US
stateOrProvinceName         = State
stateOrProvinceName_default = OR
localityName                = City
localityName_default        = Portland
commonName                  = Common Name
commonName_default          = *.$DOMAIN
[ v3_req ]
basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
subjectAltName = @alt_names
[alt_names]
DNS.1 = $DOMAIN
DNS.2 = *.$DOMAIN
EOL

# Create Private RSA Key
openssl genrsa -out "$ETCSSL/$DOMAIN".key 1024

# Create Certifcate Signing Request
openssl req -new -key "$ETCSSL/$DOMAIN".key -out "$ETCSSL/$DOMAIN".csr -config "$OpenSSLConf"

# Create Certifcate
openssl x509 -req -days 365 -in "$ETCSSL/$DOMAIN".csr \
-signkey "$ETCSSL/$DOMAIN".key -out "$ETCSSL/$DOMAIN".crt \
-extensions v3_req \
-extfile "$OpenSSLConf"

# Nix the configfile
rm -- "$OpenSSLConf"

echo -e ${GREEN}Done.${NC}
