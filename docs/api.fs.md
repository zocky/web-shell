## FS Methods
These is the specifaction of methods that FS Adapters have to implement. 
These is the WebShell-facing API, how adapters communicate with their
backend is up to adapter developers. 

### `fs.stat { path }`
````js
{ 
  String pid,   // parent id, in most cases the parent's path
  String id,    // file id, in most cases the path
  String name, 
  String type,
  Number size
  Date ctime, 
  Date mtime, 
  Date atime,
  Object is: { Boolean folder, Boolean link }, 
}
````

### `fs.ls { path }`
````js
{ 
  ...stat,
  [Buffer|String] content,
  String encoding // text|buffer|base64|json
}
````

### `fs.read { path }`
````
{ 
  ... stat,
  content,
  encoding,
}
````
