 const GoogleConnector = class {
    constructor ({
      apiKey,
      clientId,
      scope = 'metadata.readonly',
      discoveryDocs = 'https://www.googleapis.com/discovery/v1/apis/drive/v3/rest',
      onFail = ()=>{},
      onLoginFail = ()=>{},
      onAuthorizationFail = ()=>{},
      onSuccess = ()=>{},
      onLoginSuccess = ()=>{},
      onAuthorizationSuccess = ()=>{},
    }) {
      Object.assign(this,{
        apiKey,
        clientId,
        scope: 'https://www.googleapis.com/auth/'+scope,
        discoveryDocs,
        onFail,
        onLoginFail,
        onAuthorizationFail,
        onSuccess,
        onLoginSuccess,
        onAuthorizationSuccess,
      })
      this.GoogleAuth = null;
      this.connect();
    } 
    
    connect() {
      console.log('connecting')
      gapi.load('client:auth2', () => {
        gapi.client.init({
          'discoveryDocs': [this.discoveryDocs],
          'scope': this.scope,
          'apiKey': this.apiKey,
          'clientId': this.clientId,
        }).then(() => {
          this.GoogleAuth = gapi.auth2.getAuthInstance();
          this.GoogleAuth.isSignedIn.listen(this.onLoginStatus.bind(this));
          this.onLoginStatus();
        });
      });
    }

    onLoginStatus() {
      var user = this.user = this.GoogleAuth.currentUser.get();
      if (!user) {
        this.onLoginFail();
        this.onFail();
        return;
      } 
      this.onLoginSuccess(user);

      var isAuthorized = user.hasGrantedScopes(this.scope);
      if (!isAuthorized) {
        this.onAuthorizationFail();
        this.onFail();
        return;
      }
      this.onAuthorizationSuccess(user);
      this.onSuccess(user);
    }

    disconnect() {
      this.GoogleAuth.disconnect();
    }
    signIn() {
      this.GoogleAuth.signIn();
    }
    signOut() {
      this.GoogleAuth.signOut();
    }
  }