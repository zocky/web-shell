const sessionsFile = __dirname + '/sessions/sessions.json';
const randomString = require('crypto-random-string');
const util = require('util');
const bcrypt = require('bcrypt');
const fs = require('fs').promises;

module.exports = function(opt) {
  var sessions = new Sessions(opt);
  sessions.load();
  return sessions;
}

async function getValue(req,val,def) {
  if (val===undefined) return def;
  if (typeof val==='function') return await val(req);
  return val;
}

class Sessions {
  constructor ({verifyUser, getUser = (username)=>({username}), file='./sessions.json',loginRedirect='/',loginFailRedirect=loginRedirect,logoutRedirect=loginRedirect,cookiePath=loginRedirect}) {
    Object.assign(this,{
      data: {},
      file,
      verifyUser,
      getUser,
      loginRedirect,
      cookiePath,
      loginFailRedirect,
      logoutRedirect
    })

    this.auth = this.auth.bind(this);
    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
  }
  
  async verifyUser(req) {
    return null;
  }
  async getUser(username) {
    return null;
  }
  async auth (req,res,next) {
    const {token,permit} = req.cookies;
    if (!token) return next && next();
    if (!this.data[token]) return next && next();
    const valid = await bcrypt.compare(permit,this.data[token].hash);
    if (!valid) return next && next();
    req.session = this.data[token];
    console.log('auth')
    req.user = await this.getUser(req.session.username);
    console.log('auth',{token,permit})
    next && next()
  }
  async login(req,res,next) {
    const username = await this.verifyUser(req);
    if (!username) {
      const cookiePath = await getValue(req,this.cookiePath);
      console.log('cookiePath',cookiePath)
      res.writeHead(303, {
        'Location': await getValue(req,this.loginFailRedirect),
        'Set-Cookie': [
          'token=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path='+cookiePath,
          'path=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path='+cookiePath,
        ]
      });
      res.end();
      return;
    }
    const {session,permit} = await this.create(username,{});
    req.session = session;
    const cookiePath = await getValue(req,this.cookiePath);
    console.log('good cookiePath',cookiePath)
    const loginRedirect = await getValue(req,this.loginRedirect)
    if (loginRedirect) {
      res.writeHead(303, {
        'Location': loginRedirect,
        'Set-Cookie': [
          'token='+session.token+'; HttpOnly; SameSite; path='+cookiePath,
          'permit='+permit+'; HttpOnly; SameSite; path='+cookiePath
        ]
      });
      res.end();
    } else {
      res.set({
        'Set-Cookie': [
          'token='+session.token+'; HttpOnly; SameSite; path='+cookiePath,
          'permit='+permit+'; HttpOnly; SameSite; path='+cookiePath
        ]
      });
      next && next();
    }
  }
  async logout(req,res,next) {
    console.log('logging out')
    const cookiePath = await getValue(req,this.cookiePath);
    const logoutRedirect =  await getValue(req,this.logoutRedirect);
    console.log('logging out2')
    if (req.session) this.delete(req.session.token);
    res.writeHead(303, {
      'Location':logoutRedirect,
      'Set-Cookie': [
        'token=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path='+cookiePath,
        'path=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path='+cookiePath,
      ]
    });
    res.end();
  }
  async create (username,data={}) {
    const token = randomString({length:20,type:"url-safe"});
    const permit = randomString({length:20,type:"url-safe"});
    const hash = await bcrypt.hash(permit,10);
    this.data[token] = {username, data, token, hash };
    this.save();
    return {session:this.data[token],permit:permit};
  }
  async delete (token) {
    delete this.data[token];
    this.save();
  }
  async load () {
    if (!this.file) return;
    try {
      this.data = JSON.parse(await fs.readFile(this.file));
      console.log('Sessions read success.')
    } catch (err) {
      console.log('Sessions read failed. Resetting.')
      this.data = {};
      this.save();
    }
  }
  async save () {
    if (!this.file) return;
    await fs.writeFile(this.file,JSON.stringify(this.data,null,2))
  }
}