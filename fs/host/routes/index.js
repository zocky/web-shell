var express = require('express');
var router = express.Router();
const sessionServer= require("../session-server/get-session-server.js");

/* GET home page. */
router.get('/', function(req, res, next) {
  console.log('home')
  res.render('welcome');
});

router.post('/', 
  (req, res, next)=> {
    return req.app.sessions.login(req,res,next);
  },
  (req,res,next) => {
    if (req.session) {
      res.render('login',{
        token:req.session.token
      });
    } else {
      res.end();
    }
  }
);

router.get('/:token', (req, res) => {
  if (!req.session || req.session.token !==req.params.token) {
    res.writeHead(302,{
      Location: "/"
    });
    return res.end();
  }
  res.render('fs',{
    username:req.session.username,
    host:req.hostname,
    mount:req.session.username+'@'+req.hostname,
    token:req.session.token
  })
});



router.post('/:token', (req, res, next) => {
  if (req.body.logout) return req.app.sessions.logout(req,res,next);
});

router.use('/:token/:cmd', async function(req, res, next) {
  if (!req.session || req.session.token !==req.params.token) {
    res.writeHead(302,{
      Location: "/"
    });
    return res.end();
  }

  const cmd = req.params.cmd;
  const method = req.method;
  const args = {};
  
  if (method==='POST') {
    Object.assign(args,req.body);
  }
  Object.assign(args,req.query);
    
  try {
    const server = await sessionServer.get(req.session);
    const result = await server.send(method,cmd,args);

    res.writeHead(200,{
      'content-type':'text/plain'
    })
    res.end(JSON.stringify({result},null,2));
  } catch (error) {
    console.log("session server returned",error)
    res.writeHead(error.status||500,{
      'content-type':'text/plain'
    })
    res.end(JSON.stringify({error},null,2));
  }

});

module.exports = router; 

async function apiHandler({method,session,cmd,args}) {
  const server = await getSessionServer(session);
  return await server.send(method,cmd,args);
}

