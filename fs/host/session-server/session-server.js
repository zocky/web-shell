const util = require("util");
const fs = require("fs").promises;
const Path = require("path");
const mime = require('mime-types')
const mmm = require("mmmagic")

const Magic = mmm.Magic;

var magic = new Magic(mmm.MAGIC_MIME_TYPE | mmm.MAGIC_SYMLINK  | mmm.MAGIC_PRESERVE_ATIME  );
const detectFile = function(...args) {
  return new Promise((resolve,reject)=>{
    magic.detectFile(...args,(error,result)=>{
      error ? reject(error) : resolve(result);
    })
  })
}
console.log('Starting session server.')

const API = {};

process.on('message',async (msg) =>{
  const {rsvp,method,path,args={}} = msg;
  console.log(msg,msg.path,path,API[path],API.ls)
  if (!API[path]) {
    process.send({re:rsvp,error:{message:"path not found "+path,status:404}})
    console.log("path not found "+path)
  } else {
    try {
      var result = await API[path](args);
      process.send({re:rsvp, result})
    } catch(error) {
      console.log(error)
      process.send({re:rsvp, error: {message: error.message || String(error), status: error.status || 500}})
    }
  }
})

async function stat(filepath) {
  filepath = Path.resolve('/',filepath);
  const parsed = Path.parse(filepath);
  var fileInfo = await fs.stat(filepath);
  var lstat = await fs.lstat(filepath);
  var type = mime.lookup(filepath);
  if (!type) type = await detectFile(filepath);
  if (!type) type = "unknown"
  var obj ={
    name:parsed.base,
    id:filepath,
    type: type === 'inode/directory' ? 'folder/directory' : type,
    size: !fileInfo.isDirectory() ? fileInfo.size : undefined,
    atime:fileInfo.atime,
    ctime:fileInfo.ctime,
    mtime:fileInfo.mtime,
    is: {
      link: lstat.isSymbolicLink() || undefined,
      folder: fileInfo.isDirectory() || undefined,
      hidden: parsed.base.startsWith('.') || undefined
    },
  }
  
  return obj;
}

API.ls = async function({path}) {
  var files = await fs.readdir(path,{withFileTypes:true});
  var ret = []
  for (const file of files) {
    try {
      const filepath = Path.resolve(path,file.name);
      const obj = await stat(filepath);
      ret.push(obj)
    } catch (error) {
      console.error(error)
    }
  }
  return ret;
}

API.read = async function({path}) {
  const fileInfo = await stat(path);
  var content = await fs.readFile(path,{ encoding: 'utf8' });
  return Object.assign(fileInfo,{content});
}

API.write = async function({path,content}) {
  path = Path.resolve('/',path);
  await fs.writeFile(path,content,"utf-8");
  return await stat(path);
}

API.mkdir = async function({path}) {
  await fs.mkdir(path);
  return await stat(path);
}