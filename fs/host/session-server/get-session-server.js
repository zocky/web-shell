const child_process=require("child_process");
const userid = require('userid');
const randomString = require('crypto-random-string');

const sessionServers = {};

exports.get = async function getSessionServer(session) {
  const {username,token} = session;
  const pending = {};
  if (!sessionServers[token]) {
    const server = sessionServers[token] = child_process.fork(__dirname + '/session-server.js',[],{
      uid: userid.uid(username)
    })
    server.on('close',(code,signal) => {
      console.log (`Server ${token} for user ${username} closed. Code ${code}, signal ${signal}`);
      delete sessionServers[token];
    })
    server.on('message',function({re,result,error}) {
      if (re ) {
        const p = pending[re];
        if (!p) return; 
        if (error) return p.reject(error);
        return p.resolve(result);
      }
    })
    sessionServers[token] = {
      server,
      send: function(method,path,args) {
        const rsvp = randomString({length:10});
        return new Promise((resolve,reject) => {
          server.send({rsvp,method,path,args});
          pending[rsvp] = {resolve,reject};
        })
      }
    }
  }
  return sessionServers[token];
}
