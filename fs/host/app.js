var util = require("util");
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const checkPassword = util.promisify(require('passwd-linux').checkPassword);

var indexRouter = require('./routes/index');

var app = express();


var sessions = require('./lib/sessions');


const FS_PATH = process.env.FS_PATH || "/";
const FS_PORT = process.env.FS_PORT || 34258;
const ROOT = path.resolve(__dirname,'../..');
process.on('uncaughtException',function(err){
  console.log(err);
})

async function verifyUser(req,res) {
  //await formParser(req,res);
  let {username,password} = req.body;
  console.log('pass',username,password)
  if (await checkPassword(username,password)) return username;
  return null;
}

app.sessions = sessions({
  file:ROOT+'/var/sessions/sessions2.json',
  verifyUser,
  cookiePath: req=>FS_PATH+req.session.token,
  loginRedirect: null,
  loginFailRedirect: FS_PATH,
  logoutRedirect: req=>FS_PATH+req.session.token,
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
app.set('port',FS_PORT)

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(app.sessions.auth)
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter, (res)=>{
  res.end();
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
