const https=require('https');
const http=require('http');
const fs = require('fs').promises;
const express=require('express');
const proxy = require('http-proxy-middleware');
const path=require('path');
const log = console.log.bind(console,"[WebShell]");

const PORT = process.env.PORT = process.env.PORT || 34257;
const FS_PORT = process.env.FS_PORT || 34258;
const DOMAIN = process.env.DOMAIN = process.env.DOMAIN || "127-0-0-1.sslip.io";

const HTTP = process.env.HTTP || false;
const ROOT = path.resolve(__dirname,'..');
const KEY = ROOT +'/etc/ssl/'+DOMAIN+'.key';
const CERT = ROOT +'/etc/ssl/'+DOMAIN+'.crt';

console.log({KEY,CERT});

const app = express();


init();

const fsStatic = {}
function getFsStatic(id) {
  fsStatic[id] = fsStatic[id] || express.static(ROOT+'/fs/'+id)
  return fsStatic[id];
}


const appStatic = {}
function getAppStatic(id) {
  appStatic[id] = appStatic[id] || express.static(ROOT+'/apps/'+id)
  return appStatic[id];
}
const assets = express.static(ROOT+'/static/assets');
const js = express.static(ROOT+'/static/js');

const fshost = proxy({
  target: 'http://localhost:'+FS_PORT,
});

const Bundler = require('parcel-bundler');
const desktop = new Bundler(ROOT+'/client/index.html',{
  outDir:ROOT+'/client/dist',
  cacheDir:ROOT+'/client/.cache',
  publicUrl: '/',
  https: HTTP ? undefined : {
    key: KEY,
    cert: CERT,
  }
}).middleware();

async function init() {
  log ("Starting Web Shell");
  if (!HTTP) {
    try {
      var key = await fs.readFile(KEY);
      var cert = await fs.readFile(CERT);
    } catch(error) {
      console.log("Couldn't read the certificate for this domain. Run bin/create-certificate.sh");
    }
    https.createServer({key,cert},app)
    .listen(PORT, error => {
      if (error) {
        log(error);
        process.exit(1);
      }
      log(`HTTP Server listening at https://desktop.${DOMAIN}:${PORT}`)
    })
  } else {
    http.createServer(app)
    .listen(PORT, 'localhost', error => {
      if (error) {
        log(error);
        process.exit(1);
      }
      log(`HTTP Server listening at http://desktop.${DOMAIN}:${PORT}`)
    })
  }

  app.use('/',(req,res,next)=>{
    if (req.hostname.endsWith('.'+DOMAIN)) {
      const subs = req.hostname.slice(0,-1-DOMAIN.length).split('/');
      if (subs.length !== 1) return res.end(404);
      const sub = subs[0];
      if (sub == 'desktop') {
        if (req.url.startsWith('/assets/')) {
          req.url = req.url.substr(7);
          return assets(req,res,next);
        }
        return desktop(req,res,next);
      }
      if (sub == 'fs-host') {
        if (req.url.startsWith('/js/')) {
          req.url = req.url.substr(3);
          return js(req,res,next);
        }
        return fshost(req,res,next);
      }
      if (sub.startsWith('fs-')) {
        if (req.url.startsWith('/js/')) {
          req.url = req.url.substr(3);
          return js(req,res,next);
        }
        const app = sub.substr(3);
        console.log('fs',app)
        return getFsStatic(app)(req,res,next);
      }
      if (sub.startsWith('app-')) {
        if (req.url.startsWith('/js/ShellApp.js')) {
          req.url = req.url.substr(3);
          return js(req,res,next);
        }
        const app = sub.substr(4);
        console.log('app',app)
        return getAppStatic(app)(req,res,next);
      }
    }
    res.end(404);
  })
}


